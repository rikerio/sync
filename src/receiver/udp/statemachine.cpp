#include "receiver/udp/statemachine.h"

#include "spdlog/spdlog.h"

using namespace automation::sync::receiver::udp;

StateMachine::StateMachine(uint16_t config_timeout, bool config_auto_ack) :
    sm(State::Wait),
    config_timeout(config_timeout),
    config_auto_ack(config_auto_ack) {

    sm.on(State::Wait, std::bind(&StateMachine::on_wait, this));
    sm.on(State::Read, std::bind(&StateMachine::on_read, this));
    sm.on(State::Error, std::bind(&StateMachine::on_error, this));

    auto log_transition = [&](State from, State to) {
        if (from == to) {
            return;
        }
        static const std::vector<std::string> labels({ "wait", "read", "error" });
        unsigned int from_idx = static_cast<unsigned int>(from);
        unsigned int to_idx = static_cast<unsigned int>(to);
        spdlog::debug("receiver::udp::statemachine transition from {} to {}.", labels[from_idx], labels[to_idx]);
    };

    sm.on(log_transition);

}

void StateMachine::tcp_ready() {
    cmd_tcp_ready = true;
}

void StateMachine::start() {
    cmd_start = true;
}

void StateMachine::stop() {
    cmd_stop = true;
}

void StateMachine::valid_msg_received() {
    cmd_valid_msg_received = true;
}

void StateMachine::ack() {
    cmd_ack = true;
}

bool StateMachine::is_ready() const {
    return sm.get_current_state() == State::Wait || sm.get_current_state() == State::Read;
}

bool StateMachine::is_error() const {

    return sm.get_current_state() == State::Error || sm.get_current_state() == State::Wait;

}

bool StateMachine::is_reqack() const {
    return sm.get_current_state() == State::Error && !config_auto_ack;
}


void StateMachine::loop() {

    sm.execute_transition();

    cmd_start = false;
    cmd_stop = false;
    cmd_ack = false;
    cmd_valid_msg_received = false;

}

void StateMachine::on_wait() {

    emit(Event::Wait);

    if (cmd_start) {
        sm.set_next_state(State::Read);
    }

}

void StateMachine::on_read() {

    emit(Event::Read);

    if (sm.is_first_call()) {
        timer_timeout.reset();
    }

    if (cmd_valid_msg_received) {
        timer_timeout.reset();
        spdlog::debug("receiver::udp::statemachine valid message receiver.");
    }

    if (timer_timeout.is_time_past(config_timeout)) {
        sm.set_next_state(State::Error);
        spdlog::debug("receiver::udp::statemachine timeout occured.");
        emit(Event::Error);
        return;
    }

    if (cmd_stop) {
        sm.set_next_state(State::Wait);
        return;
    }


}

void StateMachine::on_error() {

    if (config_auto_ack || cmd_ack) {
        sm.set_next_state(State::Wait);
        return;
    }

}
