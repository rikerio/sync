#ifndef __RIKERIO_SYNC_UTILS_TRIGGER_H__
#define __RIKERIO_SYNC_UTILS_TRIGGER_H__

#include "utils/event-emitter.h"

namespace automation::utils {

class Trigger : public utils::EventEmitter {

public:

    enum class Event {
        Rising, Falling
    };

    Trigger();

    void set_value(bool);
    void loop();

private:

    bool current_value = false;
    bool old_value = false;

};


}


#endif
