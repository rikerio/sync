#ifndef __RIKERIO_SYNC_CONFIG_DATA_H__
#define __RIKERIO_SYNC_CONFIG_DATA_H__

#include "string"
#include "vector"

namespace automation::sync::config {

struct Data {

    struct Item {
        std::string source;
        std::string target;
        std::string type;
    };

    std::vector<Item> item_list;

    static void deserialize(const std::string&, Data&);

};


}


#endif
