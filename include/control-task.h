#ifndef __SYNC_CONTROL_TASK_H__
#define __SYNC_CONTROL_TASK_H__

#include "rikerio-automation/automation.h"

namespace automation::sync {

class ControlTask : public automation::os::TaskInterface {

public:
    ControlTask(
        const std::string&,
        const std::string&,
        automation::os::CyclicThreadInterface&);

    void init();
    void loop();
    void quit();


private:

    automation::os::CyclicThreadInterface&  	thread;

    automation::Context                         context;

    //Module::Command<uint16_t>              		module_command;

    automation::rikerio::Uint16::Data		    io_inout_command;
    automation::rikerio::Uint8::Data            io_out_state; // 0 = offline, 1 = online, 2 = running
    automation::rikerio::Uint32::Data           io_out_max_cycle;
    automation::rikerio::Uint32::Data           io_out_last_cycle;
    automation::rikerio::Uint32::Data           io_out_avg_cycle;
    automation::rikerio::Uint32::Data           io_out_scheduled_cycle;

private:

    void                                        handle_command();

};

}



#endif
