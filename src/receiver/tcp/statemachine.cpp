#include "receiver/tcp/statemachine.h"

#include "spdlog/spdlog.h"
#include "string"

using namespace automation::sync::receiver::tcp;

StateMachine::StateMachine(unsigned int ping_cycle_count) :
    sm(State::WaitForClient),
    ping_cycle_count(ping_cycle_count) {

    sm.on(State::WaitForClient, std::bind(&StateMachine::on_wait_for_client, this));
    sm.on(State::Ready, std::bind(&StateMachine::on_ready, this));

    auto log_transition = [&](State from, State to) {
        if (from == to) {
            return;
        }
        static const std::vector<std::string> labels({ "wait", "ready" });
        unsigned int from_idx = static_cast<unsigned int>(from);
        unsigned int to_idx = static_cast<unsigned int>(to);
        spdlog::debug("receiver::tcp transition from {} to {}.", labels[from_idx], labels[to_idx]);
    };

    sm.on(log_transition);

}

void StateMachine::client_connected() {
    cmd_client_connect = true;
}

void StateMachine::client_disconnected() {
    cmd_client_disconnect = true;
}

void StateMachine::loop() {

    sm.execute_transition();

}

bool StateMachine::is_ready() const {
    return sm.get_current_state() == State::Ready;
}

void StateMachine::on_wait_for_client() {

    emit(Event::Wait);

    if (cmd_client_connect) {
        cycle_counter = 0;
        sm.set_next_state(State::Ready);
        emit(Event::SendReset);
    }

    cmd_client_connect = false;
    cmd_client_disconnect = false;

}

void StateMachine::on_ready() {

    emit(Event::Ready);

    if (cycle_counter++ == ping_cycle_count) {
        emit(Event::SendPing);
        cycle_counter = 0;
    } else if (cmd_client_disconnect) {
        sm.set_next_state(State::WaitForClient);
    }

    cmd_client_connect = false;
    cmd_client_disconnect = false;

}
