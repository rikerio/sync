#ifndef __SYNC_SENDER_TASK_H__
#define __SYNC_SENDER_TASK_H__

#include "memory"

#include "rikerio-automation/automation.h"
#include "read-job.h"
#include "arpa/inet.h"

#include "config/main.h"
#include "config/data.h"

#include "netinet/in.h"
#include "sender/udp/statemachine.h"
#include "sender/udp/client.h"
#include "sender/tcp/statemachine.h"
#include "sender/tcp/client.h"

using namespace std;

namespace automation::sync::sender {

class Task :
    public automation::os::TaskInterface {

public:

    Task(config::Main&, config::Data&, bool test = false);

    ~Task();

    void init();
    void loop();
    void quit();

private:

    automation::Context 	ctx;

    config::Main            config_main;
    config::Data			config_data;

private:

    struct {
        struct {
            struct {
                automation::rikerio::Bool::Data running;
            } flag;
        } out;
    } io;

private:

    template<typename T>
    using ptr_vector = std::vector<std::shared_ptr<T>>;

    ptr_vector<ReadJob>		read_job_list;

private:

    size_t					all_job_count_size = 0;

private:

    // udp stuff

    udp::StateMachine		udp_sm;
    udp::Client				udp_client;
    uint8_t*				udp_msg = nullptr;
    uint16_t				udp_msg_size = 0;
    uint16_t				udp_id = 0;
    uint16_t				udp_current_read_job_index = 0;
    bool					udp_send_error = false;
    unsigned int			udp_init_job_count = 0;

    void 					prep_udp_update();
    void 					send_udp_update();

private:

    // tcp stuff
    tcp::StateMachine		tcp_sm;
    tcp::Client				tcp_client;

    void tcp_connect();
    void tcp_wait_connect();
    void tcp_receive();
    void tcp_reset();
    void tcp_ping();
    void tcp_error();
    void tcp_closed();

};

}
#endif
