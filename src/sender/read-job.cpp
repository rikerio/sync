#include "sender/read-job.h"

#include "arpa/inet.h"

using namespace automation::sync::sender;

const map<const ReadJob::Type, const size_t> ReadJob::Size_map{
    { ReadJob::Type::Bool, sizeof(bool) },
    { ReadJob::Type::Uint8, sizeof(uint8_t) },
    { ReadJob::Type::Int8, sizeof(uint8_t) },
    { ReadJob::Type::Uint16, sizeof(uint16_t) },
    { ReadJob::Type::Int16, sizeof(int16_t) },
    { ReadJob::Type::Uint32, sizeof(uint32_t) },
    { ReadJob::Type::Int32, sizeof(int32_t) },
    { ReadJob::Type::Uint64, sizeof(uint64_t) },
    { ReadJob::Type::Int64, sizeof(int64_t) },
    { ReadJob::Type::Float, sizeof(float) },
    { ReadJob::Type::Double, sizeof(double) }
};


const std::map<const std::string, const ReadJob::Type> ReadJob::Type_str_map{
    { "bool", Type::Bool },
    { "uint8", Type::Uint8 },
    { "int8", Type::Int8 },
    { "uint16", Type::Uint16 },
    { "int16", Type::Int16 },
    { "uint32", Type::Uint32 },
    { "int32", Type::Int32 },
    { "uint64", Type::Uint64 },
    { "int64", Type::Int64 },
    { "float", Type::Float },
    { "double", Type::Double }
};


ReadJob::ReadJob(automation::rikerio::Provider& io_provider, const std::string& id, Type type):
    id(id), type(type), byte_size(Size_map.at(type)) {

    const auto flags = automation::rikerio::Provider::F_Read;

    buffer = (uint8_t*) calloc(1, byte_size);

    switch (type) {
    case (Type::Bool): {
        io_provider
        // .create_data<bool>(id, true)
        .create<bool>(id, bool_data, flags);
        break;
    }
    case (Type::Uint8): {
        io_provider
        // .create_data<uint8_t>(id, true)
        .create<uint8_t>(id, uint8_data, flags);
        break;
    }
    case (Type::Int8): {
        io_provider
        // .create_data<int8_t>(id, true)
        .create<int8_t>(id, int8_data, flags);
        break;
    }
    case (Type::Uint16): {
        io_provider
        // .create_data<uint16_t>(id, true)
        .create<uint16_t>(id, uint16_data, flags);
        break;
    }
    case (Type::Int16): {
        io_provider
        // .create_data<int16_t>(id, true)
        .create<int16_t>(id, int16_data, flags);
        break;
    }
    case (Type::Uint32): {
        io_provider
        // .create_data<uint32_t>(id, true)
        .create<uint32_t>(id, uint32_data, flags);
        break;
    }
    case (Type::Int32): {
        io_provider
        // .create_data<int32_t>(id, true)
        .create<int32_t>(id, int32_data, flags);
        break;
    }
    case (Type::Uint64): {
        io_provider
        // .create_data<uint64_t>(id, true)
        .create<uint64_t>(id, uint64_data, flags);
        break;
    }
    case (Type::Int64): {
        io_provider
        // .create_data<int64_t>(id, true)
        .create<int64_t>(id, int64_data, flags);
        break;
    }
    case (Type::Float): {
        io_provider
        // .create_data<float>(id, true)
        .create<float>(id, float_data, flags);
        break;
    }
    case (Type::Double) : {
        io_provider
        // .create_data<double>(id, true)
        .create<double>(id, double_data, flags);
        break;
    }
    }

}

ReadJob::~ReadJob() {
    free(buffer);
}

bool ReadJob::is_updated() const {
    switch (type) {
    case (Type::Bool): {
        return memcmp(&bool_data.value, buffer, byte_size) != 0;
    }
    case (Type::Uint8): {
        return memcmp(&uint8_data.value, buffer, byte_size) != 0;
    }
    case (Type::Int8): {
        return memcmp(&int8_data.value, buffer, byte_size) != 0;
    }
    case (Type::Uint16): {
        return memcmp(&uint16_data.value, buffer, byte_size) != 0;
    }
    case (Type::Int16): {
        return memcmp(&int16_data.value, buffer, byte_size) != 0;
    }
    case (Type::Uint32): {
        return memcmp(&uint32_data.value, buffer, byte_size) != 0;
    }
    case (Type::Int32): {
        return memcmp(&int32_data.value, buffer, byte_size) != 0;
    }
    case (Type::Uint64): {
        return memcmp(&uint64_data.value, buffer, byte_size) != 0;
    }
    case (Type::Int64): {
        return memcmp(&int64_data.value, buffer, byte_size) != 0;
    }
    case (Type::Float): {
        return memcmp(&float_data.value, buffer, byte_size) != 0;
    }
    case (Type::Double) : {
        return memcmp(&double_data.value, buffer, byte_size) != 0;
    }
    default:
        return false;
    }
}

void ReadJob::update() {

    switch (type) {
    case (Type::Bool): {
        memcpy(buffer, &bool_data.value, byte_size);
        break;
    }
    case (Type::Uint8): {
        memcpy(buffer, &uint8_data.value, byte_size);
        break;
    }
    case (Type::Int8): {
        memcpy(buffer, &int8_data.value, byte_size);
        break;
    }
    case (Type::Uint16): {
        memcpy(buffer, &uint16_data.value, byte_size);
        break;
    }
    case (Type::Int16): {
        memcpy(buffer, &int16_data.value, byte_size);
        break;
    }
    case (Type::Uint32): {
        memcpy(buffer, &uint32_data.value, byte_size);
        break;
    }
    case (Type::Int32): {
        memcpy(buffer, &int32_data.value, byte_size);
        break;
    }
    case (Type::Uint64): {
        memcpy(buffer, &uint64_data.value, byte_size);
        break;
    }
    case (Type::Int64): {
        memcpy(buffer, &int64_data.value, byte_size);
        break;
    }
    case (Type::Float): {
        memcpy(buffer, &float_data.value, byte_size);
        break;
    }
    case (Type::Double) : {
        memcpy(buffer, &double_data.value, byte_size);
        break;
    }
    }

}

/* copy value from shm to msg buffer for example */
size_t ReadJob::apply(uint8_t* ptr) {

    switch (type) {
    case (Type::Bool): {
        memcpy(ptr, &bool_data.value, byte_size);
        break;
    }
    case (Type::Uint8): {
        memcpy(ptr, &uint8_data.value, byte_size);
        break;
    }
    case (Type::Int8): {
        memcpy(ptr, &int8_data.value, byte_size);
        break;
    }
    case (Type::Uint16): {
        uint16_t tmp_ho = 0; // host order
        uint16_t tmp_no = 0; // network order
        memcpy(&tmp_ho, &uint16_data.value, byte_size);
        tmp_no = htons(tmp_ho);
        memcpy(ptr, &tmp_no, byte_size);
        break;
    }
    case (Type::Int16): {
        uint16_t tmp_ho = 0; // host order
        uint16_t tmp_no = 0; // network order
        memcpy(&tmp_ho, &int16_data.value, byte_size);
        tmp_no = htons(tmp_ho);
        memcpy(ptr, &tmp_no, byte_size);
        break;
    }
    case (Type::Uint32): {
        uint32_t tmp_ho = 0; // host order
        uint32_t tmp_no = 0; // network order
        memcpy(&tmp_ho, &uint32_data.value, byte_size);
        tmp_no = htonl(tmp_ho);
        memcpy(ptr, &tmp_no, byte_size);
        break;
    }
    case (Type::Int32): {
        uint32_t tmp_ho = 0; // host order
        uint32_t tmp_no = 0; // network order
        memcpy(&tmp_ho, &int32_data.value, byte_size);
        tmp_no = htonl(tmp_ho);
        memcpy(ptr, &tmp_no, byte_size);
        break;
    }
    case (Type::Uint64): {
        uint64_t tmp_ho = 0; // host order
        uint64_t tmp_no = 0; // network order
        memcpy(&tmp_ho, &uint64_data.value, byte_size);
        tmp_no = htobe64(tmp_ho);
        memcpy(ptr, &tmp_no, byte_size);
        break;
    }
    case (Type::Int64): {
        uint64_t tmp_ho = 0; // host order
        uint64_t tmp_no = 0; // network order
        memcpy(&tmp_ho, &int64_data.value, byte_size);
        tmp_no = htobe64(tmp_ho);
        memcpy(ptr, &tmp_no, byte_size);
        break;
    }
    case (Type::Float): {
        uint32_t tmp_ho = 0; // host order
        uint32_t tmp_no = 0; // network order
        memcpy(&tmp_ho, &float_data.value, byte_size);
        tmp_no = htonl(tmp_ho);
        memcpy(ptr, &tmp_no, byte_size);
        break;
    }
    case (Type::Double) : {
        uint64_t tmp_ho = 0; // host order
        uint64_t tmp_no = 0; // network order
        memcpy(&tmp_ho, &double_data.value, byte_size);
        tmp_no = htobe64(tmp_ho);
        memcpy(ptr, &tmp_no, byte_size);
        break;
    }
    }

    return byte_size;

}

size_t ReadJob::get_byte_size() const {
    return byte_size;
}
