#ifndef __RIKERIO_SYNC_SENDER_TCP_CLIENT_H__
#define __RIKERIO_SYNC_SENDER_TCP_CLIENT_H__

#include "stdexcept"
#include "string"
#include "unistd.h"
#include "sys/types.h"
#include "sys/socket.h"
#include "arpa/inet.h"

namespace automation::sync::sender::tcp {

class Client {

public:

    class connection_error : public std::runtime_error {
    public:
        connection_error(const std::string& what) : std::runtime_error(what) { };
    };
    class send_error : public std::exception { };
    class receive_error : public std::exception { };

    Client(const std::string&, uint16_t);

    void connect();
    void close();

    void send_message(uint8_t*, size_t);
    void read_message(uint8_t*, size_t*, size_t);

    bool is_connected() const;

private:

    const std::string	host;
    uint16_t			port;

    int					socket_fd;
    sockaddr_in 		address;


};


}


#endif
