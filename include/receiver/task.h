#ifndef __SYNC_RECEIVER_TASK_H__
#define __SYNC_RECEIVER_TASK_H__

#include "memory"

#include "utils/trigger.h"

#include "rikerio-automation/automation.h"
#include "config/main.h"
#include "config/data.h"

#include "receiver/write-job.h"
#include "receiver/statemachine.h"
#include "receiver/udp/server.h"
#include "receiver/udp/statemachine.h"

#include "receiver/tcp/server.h"
#include "receiver/tcp/statemachine.h"


using namespace std;

namespace automation::sync::receiver {

class Task :
    public automation::os::TaskInterface {

public:

    Task(config::Main&, config::Data&, bool test = false);

    ~Task();

    void init();
    void loop();
    void quit();

private:

    automation::Context 	        context;

    config::Main			        config_main;
    config::Data			        config_data;

    template<typename T>
    using ptr_vector = std::vector<std::shared_ptr<T>>;

    ptr_vector<WriteJob>	write_list;

    StateMachine			sm;

private:

    struct {
        struct {
            automation::rikerio::Bool::Data ack;
        } in;
        struct {
            automation::rikerio::Bool::Data running;
            automation::rikerio::Bool::Data	reqack;
            automation::rikerio::Bool::Data error;
        } out;
    } io;



private:

    // udp stuff

    udp::Server				udp_server;
    udp::StateMachine		udp_sm;

    utils::Trigger			trigger_ack;

    uint16_t				udp_expected_id = 0;

    void					udp_reset();
    void					udp_receive();
    void					udp_deserialize(uint8_t*, size_t);

private:

    tcp::Server				tcp_server;
    tcp::StateMachine		tcp_sm;

    void 					tcp_wait();
    void					tcp_ready();

};

}
#endif
