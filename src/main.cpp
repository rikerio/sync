
#include "dlfcn.h"
#include "execinfo.h"
#include "memory"

#include "config/data.h"
#include "config/exceptions.h"
#include "config/main.h"
#include "setup-data.h"

#include "CLI11.h"
#include "rikerio-automation/automation.h"
#include "spdlog/spdlog.h"

#include "common.h"
#include "control-task.h"
#include "receiver/task.h"
#include "semver.h"
#include "sender/task.h"

using namespace automation::sync;

automation::SemanticVersion semver(version);
std::string config_file = "/etc/rikerio/sync/config.default.yaml";
std::string data_file = "/etc/rikerio/sync/data.default.yaml";
bool setup_data = false;
bool verbose_mode = false;

std::shared_ptr<config::Main> config_main;
std::shared_ptr<config::Data> config_data;

automation::os::Eventbus eventbus;

automation::os::SignalHandler term_sh({SIGTERM, SIGINT}, []() {
  spdlog::info("SIGTERM/SIGINT Signal emitted, terminating.");

  eventbus.stop();
});

automation::os::SignalHandler segv_sh({SIGSEGV, SIGABRT}, []() {
  void *array[10];
  size_t size;

  // get void*'s for all entries on the stack
  size = backtrace(array, 10);

  // print out all the frames to stderr
  spdlog::error("Error: signal {}", SIGSEGV);
  backtrace_symbols_fd(array, size, STDERR_FILENO);
  exit(1);
});

static void systemd_notify(const char *msg) {

  typedef int (*systemd_notify)(int code, const char *param);

  void *systemd_so = dlopen("libsystemd.so", RTLD_NOW);

  if (systemd_so == NULL) {
    spdlog::info("No libsystemd.so found. No SystemD notification happening.");
    return;
  }

  systemd_notify *sd_notify = (systemd_notify *)dlsym(systemd_so, "sd_notify");
  spdlog::info("Notifying systemd ({}).", msg);
  systemd_notify func = (systemd_notify)sd_notify;
  int retVal = func(0, msg);

  if (retVal < 0) {
    spdlog::error("Error notifying (sd_notify) SystemD ({}).", retVal);
  }

  dlclose(systemd_so);
}

void init() {

  spdlog::set_level(verbose_mode ? spdlog::level::debug : spdlog::level::info);

  spdlog::info("Starting RikerIO Sync Version {}", semver.get_version());
  spdlog::debug("Verbose mode enabled.");
  spdlog::info("Using configuration file {}.", config_file);

  eventbus.reg(term_sh);
  eventbus.reg(segv_sh);
}

void start_sender() {

  spdlog::info("Application started as Sender.");

  config::Main config_main;

  try {
    config::Main::deserialize(config_file, config_main);
  } catch (config::Error &e) {
    spdlog::error(e.get_message());
    spdlog::info("Exiting application.");
    exit(1);
  }

  spdlog::info("Using data file {}.", data_file);

  config::Data config_data;

  try {
    config::Data::deserialize(data_file, config_data);
  } catch (config::Error &e) {
    spdlog::error(e.get_message());
    spdlog::info("Exiting application.");
    exit(1);
  }

  SetupData setup_data_instance(config_main.sender, config_data, true);

  if (setup_data) {
    spdlog::info("Setting up Data Points in RikerIO Profile.");
    setup_data_instance.init();
  }

  automation::sync::sender::Task main_task(config_main, config_data);

  automation::os::RealtimeThread realtime_thread(config_main.thread);

  realtime_thread.set_name("Runtime");

  automation::sync::ControlTask control_task(
      config_main.sender.rikerio, config_main.sender.prefix, realtime_thread);

  realtime_thread.add_task(control_task);
  realtime_thread.add_task(main_task);

  /* wait for all to be initialized before starting the RT-Task */

  spdlog::info("Starting Realtime Thread.");

  systemd_notify("READY=1");

  realtime_thread.start();

  /* wait for signals */

  eventbus.wait();

  spdlog::info("Waiting for RT thread.");

  realtime_thread.stop();

  if (setup_data) {
    setup_data_instance.quit();
  }

  systemd_notify("STOPPING=1");

  spdlog::info("Exiting.");
}

void start_receiver() {

  spdlog::info("Application started as Receiver.");

  config::Main config_main;

  try {
    config::Main::deserialize(config_file, config_main);
  } catch (config::Error &e) {
    spdlog::error(e.get_message());
    spdlog::info("Exiting application.");
    exit(1);
  }

  spdlog::info("Using data file {}.", data_file);

  config::Data config_data;

  try {
    config::Data::deserialize(data_file, config_data);
  } catch (config::Error &e) {
    spdlog::error(e.get_message());
    spdlog::info("Exiting application.");
    exit(1);
  }

  SetupData setup_data_instance(config_main.receiver, config_data, false);

  if (setup_data) {
    spdlog::info("Setting up Data Points in RikerIO Profile.");
    setup_data_instance.init();
  }

  automation::sync::receiver::Task main_task(config_main, config_data);
  automation::os::RealtimeThread realtime_thread(config_main.thread);

  realtime_thread.set_name("Runtime");

  automation::sync::ControlTask control_task(config_main.receiver.rikerio,
                                             config_main.receiver.prefix,
                                             realtime_thread);

  realtime_thread.add_task(control_task);
  realtime_thread.add_task(main_task);

  /* wait for all to be initialized before starting the RT-Task */

  spdlog::info("Starting Realtime Thread.");

  systemd_notify("READY=1");

  realtime_thread.start();

  /* wait for signals */

  eventbus.wait();

  spdlog::info("Waiting for RT thread.");

  realtime_thread.stop();

  if (setup_data) {
    setup_data_instance.quit();
  }

  systemd_notify("STOPPING=1");

  spdlog::info("Exiting.");
}

int main(int argc, char **argv) {

  automation::sync::version_major = semver.get_major();
  automation::sync::version_minor = semver.get_minor();
  automation::sync::version_patch = semver.get_patch();

  CLI::App cli_app{"RikerIO Sync"};

  cli_app.add_option("-c,--config", config_file, "Config File.");
  cli_app.add_option("-d,--data", data_file, "Data File.");
  cli_app.add_flag("--setup-data", setup_data, "Setup Data Points in RikerIO.");
  cli_app.add_flag("-v,--verbose", verbose_mode, "Show debug messages.");
  auto sender_app =
      cli_app.add_subcommand("sender", "Start as Sender Application.");
  auto receiver_app =
      cli_app.add_subcommand("receiver", "Start as Receiver Application.");
  auto version_app = cli_app.add_subcommand("version", "Print Version.");

  cli_app.require_subcommand();

  version_app->final_callback([]() {
    std::cout << semver.get_version() << std::endl;
    exit(EXIT_SUCCESS);
  });

  sender_app->fallthrough();
  sender_app->final_callback([]() {
    try {
      init();
      start_sender();
    } catch (std::runtime_error &e) {
      spdlog::error("{}", e.what());
      exit(EXIT_FAILURE);
    }
  });

  receiver_app->fallthrough();
  receiver_app->final_callback([]() {
    try {
      init();
      start_receiver();
    } catch (std::runtime_error &e) {
      spdlog::error("{}", e.what());
      exit(EXIT_FAILURE);
    }
  });

  CLI11_PARSE(cli_app, argc, argv);
}
