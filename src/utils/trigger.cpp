#include "utils/trigger.h"

using namespace automation::utils;

Trigger::Trigger() { }

void Trigger::set_value(bool value) {
    current_value = value;
}

void Trigger::loop() {

    if (old_value && !current_value) {
        emit(Event::Falling);
    }

    if (!old_value && current_value) {
        emit(Event::Rising);
    }

    old_value = current_value;

}


