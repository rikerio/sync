#ifndef __RIKERIO_SYNC_CONFIG_H__
#define __RIKERIO_SYNC_CONFIG_H__

#include "string"
#include "rikerio-automation/automation.h"

namespace automation::sync::config {

struct Com {
    unsigned int cycle;
    unsigned int udp_timeout_count;
    unsigned int tcp_timeout_count;
    unsigned int ping_cycle_count;
    unsigned int ping_timeout_count;
    unsigned int max_payload_size;
};

struct Participant {

    unsigned int cycle;
    std::string host;
    unsigned int port;
    std::string prefix;
    std::string rikerio;
    bool auto_ack;

};

struct Sender : public Participant {};
struct Receiver : public Participant {};

struct Main {

    automation::os::ThreadConfig thread;
    Com com;
    Participant sender;
    Participant receiver;

    static void deserialize(const std::string&, Main&);

};



}

#endif
