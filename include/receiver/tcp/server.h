#ifndef __RIKERIO_SYNC_RECEIVER_TCP_SERVER_H__
#define __RIKERIO_SYNC_RECEIVER_TCP_SERVER_H__

#include "sys/types.h"
#include "sys/socket.h"
#include "netinet/in.h"
#include "arpa/inet.h"
#include "netdb.h"
#include "stdio.h"
#include "unistd.h"
#include "string.h"
#include "stdlib.h"
#include "errno.h"
#include "string"

namespace automation::sync::receiver::tcp {

class Server {

public:

    Server(const std::string&, const unsigned short, const size_t);
    ~Server();

    void close();
    void set_whitelist_ip(const std::string&);

    void wait_for_client(bool&);
    void write_to_socket(uint8_t*, size_t);
    void write_ping();
    void write_reset();

    unsigned int get_client_count() const;

private:

    const std::string& 		host;
    const unsigned short	port;
    const size_t			max_buffer_size;

    sockaddr_in				socket_adr;

    int						server_fd = 0;
    int						client_fd = 0;
    unsigned int			connected_client_count = 0;

    uint32_t				whitelist_ip;

};

}


#endif
