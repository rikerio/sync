#include "config/exceptions.h"

using namespace automation::sync::config;

Error::Error(const std::string& msg) : msg(msg) { }

const std::string& Error::get_message() const {
    return msg;
}
