#ifndef __RIKERIO_SYNC_RECEIVER_STATEMACHINE_H__
#define __RIKERIO_SYNC_RECEIVER_STATEMACHINE_H__

#include "utils/event-emitter.h"
#include "utils/statemachine.h"

namespace automation::sync::receiver {

class StateMachine : public utils::EventEmitter {

public:

    enum class Event {
        Init, Start, Error, NoError
    };

    StateMachine(bool);

    void set_tcp_ready(bool);
    void set_udp_ready(bool);

    void ack();

    void loop();

    bool is_error() const;
    bool is_reqack() const;

private:

    enum class State {
        Offline, Ready, Running, Error
    };

    utils::StateMachine<State>	sm;

private:

    bool						config_auto_ack = false;
    bool						cmd_tcp_ready = false;
    bool						cmd_udp_ready = false;
    bool						cmd_ack = false;

private:

    void on_offline();
    void on_ready();
    void on_running();
    void on_error();

};

}


#endif
