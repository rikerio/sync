#ifndef __RIKERIO_SYNC_RECEIVER_UDP_STATEMACHINE_H__
#define __RIKERIO_SYNC_RECEIVER_UDP_STATEMACHINE_H__

#include "rikerio-automation/automation.h"
#include "utils/statemachine.h"
#include "utils/event-emitter.h"

namespace automation::sync::receiver::udp {

class StateMachine : public utils::EventEmitter {

public:

    enum class Event {
        Wait, Read, Error
    };

public:

    StateMachine(uint16_t, bool);

    void tcp_ready();
    void start();
    void stop();
    void ack();
    void valid_msg_received();

    bool is_ready() const;
    bool is_error() const;
    bool is_reqack() const;

    void loop();


private:

    enum class State {
        Wait, Read, Error
    };

    utils::StateMachine<State>	sm;
    utils::Timer				timer_timeout;

private:

    void on_wait();
    void on_read();
    void on_error();

private:

    uint16_t					config_timeout = 1000;
    bool						config_auto_ack = false;

    bool						cmd_tcp_ready = false;
    bool						cmd_start = false;
    bool						cmd_stop = false;
    bool 						cmd_ack = false;
    bool						cmd_valid_msg_received = false;

};

}


#endif
