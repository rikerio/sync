#include "config/data.h"
#include "config/exceptions.h"
#include "yaml-cpp/yaml.h"

#include "algorithm"

using namespace automation::sync::config;

void check_type(const std::string& value) {

    static std::vector<std::string> type_list({
        "bool", "int8", "uint8", "int16", "uint16", "int32", "uint32", "int64", "uint64", "float", "double" });

    if (std::find(type_list.begin(), type_list.end(), value) != type_list.end()) {
        return;
    }

    throw Error("Unknown item type '" + value + "'.");

}

void Data::deserialize(const std::string& filename, Data& data) {

    YAML::Node root_node;

    try {
        root_node = YAML::LoadFile(filename);
    } catch (YAML::BadFile& e) {
        throw Error("File not found or not readable.");
    } catch (YAML::ParserException& e) {
        throw Error(e.what());
    }

    try {

        auto n_data = root_node["data"];

        if (!n_data || n_data.Type() != YAML::NodeType::Sequence) {
            throw Error("'data' field musst be a yaml sequence.");
        }

        for (auto n_item : n_data) {

            auto n_source = n_item["source"];
            auto n_target = n_item["target"];
            auto n_type = n_item["type"];

            if (!n_source || n_source.Type() != YAML::NodeType::Scalar) {
                throw Error("Error parsing source.");
            }

            if (!n_target || n_target.Type() != YAML::NodeType::Scalar) {
                throw Error("Error parsing target.");
            }

            if (!n_type || n_type.Type() != YAML::NodeType::Scalar) {
                throw Error("Error parsing type.");
            }

            Item item = {
                n_source.as<std::string>(),
                n_target.as<std::string>(),
                n_type.as<std::string>()
            };

            check_type(item.type);

            data.item_list.push_back(item);

        }

    } catch (YAML::BadConversion& e) {
        throw Error(e.what());
    }

}


