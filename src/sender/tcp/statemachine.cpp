#include "sender/tcp/statemachine.h"

#include "spdlog/spdlog.h"
#include "string"


using namespace automation::sync::sender::tcp;

StateMachine::StateMachine(unsigned int timeout_ping, bool auto_ack) :
    sm(State::Closed),
    auto_ack(auto_ack),
    timeout_receive(timeout_ping) {

    sm.on(State::Closed, std::bind(&StateMachine::on_closed, this));
    sm.on(State::Ready, std::bind(&StateMachine::on_ready, this));
    sm.on(State::Wait, std::bind(&StateMachine::on_wait, this));
    sm.on(State::Error, std::bind(&StateMachine::on_error, this));

    auto log_transition = [&](State from, State to) {
        if (from == to) {
            return;
        }
        static const std::vector<std::string> labels({ "closed", "ready", "wait", "error" });
        unsigned int from_idx = static_cast<unsigned int>(from);
        unsigned int to_idx = static_cast<unsigned int>(to);
        spdlog::debug("sender::tcp::statemachine transition from {} to {}.", labels[from_idx], labels[to_idx]);
    };

    sm.on(log_transition);

}

void StateMachine::ack() {
    cmd_ack = true;
}

void StateMachine::conn_est() {
    cmd_conn_est = true;
}

void StateMachine::conn_end() {
    cmd_conn_end = true;
}

void StateMachine::conn_err() {
    cmd_conn_err = true;
}

void StateMachine::recv_rst() {
    cmd_recv_rst = true;
}

void StateMachine::recv_png() {
    cmd_recv_png = true;
}

void StateMachine::loop() {

    sm.execute_transition();

}

void StateMachine::on_closed() {

    if (sm.is_first_call()) {
        emit(Event::Connect);
        timer_wait.reset();
    } else if (timer_wait.is_time_past(timeout_receive)) {
        emit(Event::Connect);
        timer_wait.reset();
    } else if (cmd_conn_est) {
        sm.set_next_state(State::Ready);
        timer_receive.reset();
        cmd_conn_est = false;
        return;
    } else if (cmd_conn_err) {
        sm.set_next_state(State::Wait);
        timer_wait.reset();
        cmd_conn_err = false;
        return;
    }

    emit(Event::Wait);

}

void StateMachine::on_ready() {

    emit(Event::Receive);

    if (cmd_recv_rst) {
        emit(Event::Reset);
    } else if (cmd_recv_png) {
        emit(Event::Ping);
        timer_receive.reset();
    } else if (cmd_conn_end) {
        emit(Event::Closed);
        sm.set_next_state(State::Closed);
    } else if (timer_receive.is_time_past(timeout_receive)) {
        emit(Event::Error);
        sm.set_next_state(State::Error);
    }

    cmd_recv_rst = false;
    cmd_conn_end = false;
    cmd_recv_png = false;

}

void StateMachine::on_wait() {

    if (timer_wait.is_time_past(1000)) {

        emit(Event::Connect);
        sm.set_next_state(State::Closed);

    }

}

void StateMachine::on_error() {

    if (auto_ack || cmd_ack) {
        sm.set_next_state(State::Closed);
    }

    cmd_ack = false;

}
