#ifndef __RIKERIO_SYNC_CONFIG_ERROR_H__
#define __RIKERIO_SYNC_CONFIG_ERROR_H__

#include "string"

namespace automation::sync::config {

class Error : public std::exception {
public:
    Error(const std::string&);
    const std::string& get_message() const;
private:
    const std::string msg;
};


}



#endif
