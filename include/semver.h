#ifndef __SEMVER_H__
#define __SEMVER_H__

#include "stdexcept"
#include "string"
#include "vector"

class SemanticVersion {

public:

    SemanticVersion(const std::string v) {

        if (v.length() == 0) {
            throw std::invalid_argument("String musst be of form x.y.z");
        }

        if (v[0] == 'v') {
            orig = v.substr(1);
        } else {
            orig = v;
        }

        std::string tmp_str = "";
        std::vector<std::string> results;
        for (unsigned int i = 0; i < orig.length(); i += 1) {
            if (orig[i] == '.') {
                results.push_back(tmp_str);
                tmp_str = "";
                continue;
            }
            tmp_str += orig[i];
        }
        results.push_back(tmp_str);

        if (results.size() > 0) {
            major = stoul(results[0]);
        }
        if (results.size() > 1) {
            minor = stoul(results[1]);
        }
        if (results.size() > 2) {
            patch = stoul(results[2]);
        }

        version = std::to_string(major) + "." + std::to_string(minor) + "." + std::to_string(patch);

    }

    unsigned int get_major() const {
        return major;
    }

    unsigned int get_minor() const {
        return minor;
    }

    unsigned int get_patch() const {
        return patch;
    }

    const std::string& get_version() const {
        return version;
    }

private:

    std::string orig;
    std::string version;
    unsigned int major = 0;
    unsigned int minor = 0;
    unsigned int patch = 0;
};

#endif
