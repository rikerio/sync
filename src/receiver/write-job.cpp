#include "receiver/write-job.h"

#include "rikerio-automation/automation.h"
#include "arpa/inet.h"

using namespace automation::sync::receiver;

const map<const WriteJob::Type, const size_t> WriteJob::Size_map{
    { WriteJob::Type::Bool, sizeof(bool) },
    { WriteJob::Type::Uint8, sizeof(uint8_t) },
    { WriteJob::Type::Int8, sizeof(uint8_t) },
    { WriteJob::Type::Uint16, sizeof(uint16_t) },
    { WriteJob::Type::Int16, sizeof(int16_t) },
    { WriteJob::Type::Uint32, sizeof(uint32_t) },
    { WriteJob::Type::Int32, sizeof(int32_t) },
    { WriteJob::Type::Uint64, sizeof(uint64_t) },
    { WriteJob::Type::Int64, sizeof(int64_t) },
    { WriteJob::Type::Float, sizeof(float) },
    { WriteJob::Type::Double, sizeof(double) }
};


const std::map<const std::string, const WriteJob::Type> WriteJob::Type_str_map{
    { "bool", Type::Bool },
    { "uint8", Type::Uint8 },
    { "int8", Type::Int8 },
    { "uint16", Type::Uint16 },
    { "int16", Type::Int16 },
    { "uint32", Type::Uint32 },
    { "int32", Type::Int32 },
    { "uint64", Type::Uint64 },
    { "int64", Type::Int64 },
    { "float", Type::Float },
    { "double", Type::Double }
};


WriteJob::WriteJob(automation::rikerio::Provider& provider, const std::string& id, Type type):
    id(id), type(type) {

    const auto flags = automation::rikerio::Provider::F_Write;


    switch (type) {
    case (Type::Bool): {
        provider
        // .create_data<bool>(id, true)
        .create<bool>(id, bool_data, flags);
        break;
    }
    case (Type::Uint8): {
        provider
        // .create_data<uint8_t>(id, true)
        .create<uint8_t>(id, uint8_data, flags);
        break;
    }
    case (Type::Int8): {
        provider
        // .create_data<int8_t>(id, true)
        .create<int8_t>(id, int8_data, flags);
        break;
    }
    case (Type::Uint16): {
        provider
        // .create_data<uint16_t>(id, true)
        .create<uint16_t>(id, uint16_data, flags);
        break;
    }
    case (Type::Int16): {
        provider
        // .create_data<int16_t>(id, true)
        .create<int16_t>(id, int16_data, flags);
        break;
    }
    case (Type::Uint32): {
        provider
        // .create_data<uint32_t>(id, true)
        .create<uint32_t>(id, uint32_data, flags);
        break;
    }
    case (Type::Int32): {
        provider
        // .create_data<int32_t>(id, true)
        .create<int32_t>(id, int32_data, flags);
        break;
    }
    case (Type::Uint64): {
        provider
        // .create_data<uint64_t>(id, true)
        .create<uint64_t>(id, uint64_data, flags);
        break;
    }
    case (Type::Int64): {
        provider
        // .create_data<int64_t>(id, true)
        .create<int64_t>(id, int64_data, flags);
        break;
    }
    case (Type::Float): {
        provider
        // .create_data<float>(id, true)
        .create<float>(id, float_data, flags);
        break;
    }
    case (Type::Double) : {
        provider
        // .create_data<double>(id, true)
        .create<double>(id, double_data, flags);
        break;
    }
    }

}

void WriteJob::apply(uint8_t* ptr) {

    size_t size = Size_map.at(type);

    switch (type) {
    case (Type::Bool): {
        memcpy(&bool_data.value, ptr, size);
        break;
    }
    case (Type::Uint8): {
        memcpy(&uint8_data.value, ptr, size);
        break;
    }
    case (Type::Int8): {
        memcpy(&int8_data.value, ptr, size);
        break;
    }
    case (Type::Uint16): {
        uint16_t tmp_ho = 0; // host order
        uint16_t tmp_no = *(uint16_t*) ptr; // network order
        tmp_ho = ntohs(tmp_no);
        memcpy(&uint16_data.value, &tmp_ho, size);
        break;
    }
    case (Type::Int16): {
        uint16_t tmp_ho = 0; // host order
        uint16_t tmp_no = *(uint16_t*) ptr; // network order
        tmp_ho = ntohs(tmp_no);
        memcpy(&int16_data.value, &tmp_ho, size);
        break;
    }
    case (Type::Uint32): {
        uint32_t tmp_ho = 0; // host order
        uint32_t tmp_no = 0;
        memcpy(&tmp_no, ptr, sizeof(uint32_t)); // network order
        tmp_ho = ntohl(tmp_no);
        memcpy(&uint32_data.value, &tmp_ho, size);

        break;
    }
    case (Type::Int32): {
        uint32_t tmp_ho = 0; // host order
        uint32_t tmp_no = 0;
        memcpy(&tmp_no, ptr, sizeof(uint32_t)); // network order
        tmp_ho = ntohl(tmp_no);
        memcpy(&int32_data.value, &tmp_ho, size);
        break;
    }
    case (Type::Uint64): {
        uint64_t tmp_ho = 0; // host order
        uint64_t tmp_no = *(uint64_t*) ptr; // network order
        tmp_ho = be64toh(tmp_no);
        memcpy(&uint64_data.value, &tmp_ho, size);
        break;
    }
    case (Type::Int64): {
        uint64_t tmp_ho = 0; // host order
        uint64_t tmp_no = *(uint64_t*) ptr; // network order
        tmp_ho = be64toh(tmp_no);
        memcpy(&int64_data.value, &tmp_ho, size);
        break;
    }
    case (Type::Float): {
        uint32_t tmp_ho = 0; // host order
        uint32_t tmp_no = 0;
        memcpy(&tmp_no, ptr, sizeof(uint32_t)); // network order
        tmp_ho = ntohl(tmp_no);
        memcpy(&float_data.value, &tmp_ho, size);
        break;
    }
    case (Type::Double) : {
        uint64_t tmp_ho = 0; // host order
        uint64_t tmp_no = *(uint64_t*) ptr; // network order
        tmp_ho = be64toh(tmp_no);
        memcpy(&double_data.value, &tmp_ho, size);
        break;
    }
    }

}

size_t WriteJob::get_byte_size() const {
    return Size_map.at(type);
}
