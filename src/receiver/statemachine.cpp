#include "receiver/statemachine.h"

#include "string"
#include "spdlog/spdlog.h"

using namespace automation::sync::receiver;

StateMachine::StateMachine(bool config_auto_ack) :
    sm(State::Offline),
    config_auto_ack(config_auto_ack) {

    sm.on(State::Offline, std::bind(&StateMachine::on_offline, this));
    sm.on(State::Ready, std::bind(&StateMachine::on_ready, this));
    sm.on(State::Running, std::bind(&StateMachine::on_running, this));
    sm.on(State::Error, std::bind(&StateMachine::on_error, this));

    auto log_transition = [&](State from, State to) {
        if (from == to) {
            return;
        }
        static const std::vector<std::string> labels({ "offline", "ready", "running", "error" });
        unsigned int from_idx = static_cast<unsigned int>(from);
        unsigned int to_idx = static_cast<unsigned int>(to);
        spdlog::debug("receiver::statemachine transition from {} to {}.", labels[from_idx], labels[to_idx]);
    };

    sm.on(log_transition);

}

void StateMachine::set_tcp_ready(bool value) {
    cmd_tcp_ready = value;
}

void StateMachine::set_udp_ready(bool value) {
    cmd_udp_ready = value;
}

void StateMachine::ack() {
    cmd_ack = true;
}

void StateMachine::loop() {
    sm.execute_transition();

    cmd_ack = false;
}

bool StateMachine::is_error() const {
    return sm.get_current_state() == State::Error;
}

bool StateMachine::is_reqack() const {
    return is_error() && !config_auto_ack;
}

void StateMachine::on_offline() {

    emit(Event::Init);
    sm.set_next_state(State::Ready);

}

void StateMachine::on_ready() {

    if (cmd_tcp_ready && cmd_udp_ready) {
        emit(Event::Start);
        sm.set_next_state(State::Running);
    }

}

void StateMachine::on_running() {

    if (!cmd_tcp_ready || !cmd_udp_ready) {
        emit(Event::Error);
        sm.set_next_state(State::Error);
    }

}

void StateMachine::on_error() {

    if (cmd_ack) {

        emit(Event::NoError);
        sm.set_next_state(State::Ready);

    }

}
