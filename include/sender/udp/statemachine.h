#ifndef __RIKERIO_SYNC_SENDER_UDP_SM_H__
#define __RIKERIO_SYNC_SENDER_UDP_SM_H__

#include "utils/statemachine.h"
#include "utils/event-emitter.h"

using namespace automation;

namespace automation::sync::sender::udp {

class StateMachine : public utils::EventEmitter {

public:

    enum class Event {
        Wait,
        Write
    };

public:

    StateMachine();

    void start();
    void stop();

    void loop();

private:

    enum class State {
        Wait, Write
    };

    utils::StateMachine<State> sm;

private:

    bool cmd_start = false;
    bool cmd_stop = false;

    void on_wait();
    void on_write();

};


}



#endif
