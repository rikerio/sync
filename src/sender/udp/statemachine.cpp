#include "sender/udp/statemachine.h"

#include "spdlog/spdlog.h"
#include "string"

using namespace automation::sync::sender::udp;

StateMachine::StateMachine() : sm(State::Wait) {

    sm.on(State::Wait, std::bind(&StateMachine::on_wait, this));
    sm.on(State::Write, std::bind(&StateMachine::on_write, this));

    auto log_transition = [&](State from, State to) {
        if (from == to) {
            return;
        }
        static const std::vector<std::string> labels({ "wait", "write" });
        unsigned int from_idx = static_cast<unsigned int>(from);
        unsigned int to_idx = static_cast<unsigned int>(to);
        spdlog::debug("sender::udp transition from {} to {}.", labels[from_idx], labels[to_idx]);
    };

    sm.on(log_transition);


}

void StateMachine::start() {

    cmd_start = true;

}

void StateMachine::stop() {

    cmd_stop = true;

}

void StateMachine::loop() {

    sm.execute_transition();

}

void StateMachine::on_wait() {

    emit(Event::Wait);
    if (cmd_start) {
        sm.set_next_state(State::Write);
    }
    cmd_start = false;
    cmd_stop = false;

}

void StateMachine::on_write() {

    emit(Event::Write);
    if (cmd_stop) {
        sm.set_next_state(State::Wait);
    }
    cmd_start = false;
    cmd_stop = false;

}
