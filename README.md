# RikerIO Sync

RikerIO Sync is a tool to sync data points between different machines via a UDP connection.

## Motivation

The Application synchronizes data points between to host in a cyclic manner. It uses a sender machine and a receiver machine for a one way data transfer. Data Points are defined in a configuation file defining a source data points that will be created on the sender and a target data point that will be created on the receiver. Both data points muss be of the same data type. 

## Sender/Receiver Configuration File Example

```
# config.yaml

data:
    - source: sync.out.axis.1.f.error
      target: sync.in.axis.1.error.3.no
      type: bool
    - source: sync.out.axis.1.f.reqack
      target: sync.in.axis.1.error.3.reqack
      type: bool
    - source: sync.out.axis.1.error.id
      target: sync.in.axis.1.error.3.id
      type: uint32
```

It is a simple as that. The data that should be transfered is defined in a YAML File listing the data point ids of the source and the target as well as the type. The type can be one of the following types:

bool
uint8
int8
uint16
int16
uint32
int32
uint64
int64
float
double

Custom byte arrays are not yet supported. You can define up to 65.535 data point entries here (uint16).

## Communication

The data is exchanged cyclic in a user defined time. Those times should be synchronized between the receiver and sender and can be set in an additional config file.

For synchronization purposes the receiver will open a TCP Port for the Sender to connect to. Only the senders IP Address is allowed to connect to the port. The TCP Connection will be used for syncronizing the communication. To start the dataflow and in case of errors the Receiver will issue a reset command to (re)start the UDP communication with a package id 0 and a fresh and complete dataset. Every X cycles the Sender will send a Ping Package through the TCP Connection and expects a Pong Response inside a defined timeframe. Otherwise the process changes into a error state and musst be reset. If the Sender Issues a Close Request then the Receiver Changes its State to 'Stopped' and closes the TCP Connection to the Sender.

In this example the application will run in 100ms cycles. When the cycle starts the sender will initially send all data to the receiver. After this the sender sends incremental updates of the data that has been changed. Each data packet will contain a running package id and a checksum. This way the receiver can keep track and check the received information for correctnes. If a package is malformed or missing then receiver stops and waits for a package with the id 0. The Receiver changes into an error state that cannot be acknowledged.

## Sender

Start a Sender from the Command Line like so 

```
rio-sync --config=config.yaml --data=data.yaml sender
```

## Receiver

Start a Receiver from the Command Line like so

```
rio-sync --config=config.yaml --data=data.yaml receiver
```

## Invalid Data Point IDs

For the Sender there are some Data Point IDs that are taken by the application itself:

Common Flags to indicate Error, Request for Acknowledgement or the Running State.

- ${prefix}out.flag.error (bool)
- ${prefix}out.flag.reqack (bool)
- ${prefix}out.flag.running (bool)

In order to acknowledge errors there is a also a boolean ack command which muss be enabled for at least
on cycle so that the application can recognize the ackownledgement.

- ${prefix}in.ack (bool)

To identify the error in general there are bools for the specific error like Timeout, TCP Connection or Sync error due to missing package ids or checksum errors.

- ${prefix}out.error.timeout (bool)
- ${prefix}out.error.connection (bool)
- ${prefix}out.error.sync.id (bool)
- ${prefix}out.error.sync.chksum (bool)

For inspecting the udp workload statistics are available to indentify the count of
send packages, the size of the last package as well as the average size of packages and
of course the already known max size of a package. Since packages can be split into multiple
parts depending on the UDP Frame Size the max size could be bigger then the allowed frame size.

- ${prefix}out.stat.send.count (uint32)
- ${prefix}out.stat.send.size.last (uint32)
- ${prefix}out.stat.send.size.avg (uint32)
- ${prefix}out.stat.send.size.max (uint32)


## UDP Protocol

From the sender to the receiver.

1. Header [ Counter | Payload Length ] 
2. Payload Description [ idx 1 | idx 2 ... ] 
3. Payload Data [ data 1 | data 2 ... ]

Counter: uint16
Payload Length: uint16
Checksum: uint16

idx ?: uint16
data ?: variable size between 1 and 8 bytes

## TCP Protocol

From the receiver to the sender

1. CMD: uint16

0x0001 = Reset 
0xFFFE = Ping
