#include "rikerio-automation/automation.h"
#include "spdlog/spdlog.h"
#include "config/main.h"
#include "config/data.h"
#include "sender/task.h"
#include "receiver/task.h"

using namespace automation::sync;
using namespace automation::rikerio;

class BoolSubmit : public automation::test::Scenario {

    struct IO {

        struct {
            Bool::Data sender;
        } out;

        struct {
            Bool::Data sender_running;
            Bool::Data receiver_running;
            Bool::Data receiver;
        } in;

    } io;

public:

    void setup(automation::test::Instance& instance) {

        auto f_write = automation::rikerio::Provider::F_Write;
        auto f_read = automation::rikerio::Provider::F_Read;

        auto& provider = context.add_provider<automation::rikerio::Provider>(instance.get_profile_path(), "test");


        provider
        //.create_data<bool>("receiver.pdo.out.flag.running", true)
        //.create_data<bool>("sender.pdo.out.flag.running", true)
        .create_data<bool>("sender.pdo.source", true)
        .create_data<bool>("receiver.pdo.target", true);

        provider
        .create<bool>("sender.pdo.source", io.out.sender, f_write)
        //.create<bool>("sender.pdo.out.flag.running", io.in.sender_running, f_read)
        //.create<bool>("receiver.pdo.out.flag.running", io.in.receiver_running, f_read)
        .create<bool>("receiver.pdo.target", io.in.receiver, f_read);

        // define test scenarios

        // on cycle 1 check that the sender and receiver are running
        /* at(1).execute([&](auto) {
            automation::test::_assert(io.in.receiver_running, "Receiver is NOT running.");
            automation::test::_assert(io.in.sender_running, "Sender is NOT running");
        })*/

        // starting with cycle 1 check that both pdos are false for the next
        at(1).execute([&](auto) {
            automation::test::_assert(!io.out.sender && !io.in.receiver);
        });

        // set the sender pdo to true
        at(2).execute([&](auto) {
            io.out.sender = true;
        });

        // check that in cycle 3 the receiver pdo is also true
        at(4).execute([&](auto) {
            automation::test::_assert(io.out.sender, "Source Value NOT Set");
            automation::test::_assert(io.in.receiver, "Target Value NOT Set");
        });

        // reset the sender pdo
        at(5).execute([&](auto) {
            io.out.sender = false;
        });

        // check the receiver pdo again
        at(7).execute([&](auto) {
            automation::test::_assert(!io.in.receiver);
        });

    }

    void tear_down() { }

};

class DoubleSubmit : public automation::test::Scenario {

    struct IO {
        Double::Data source;
        Double::Data target;

        struct {
            Bool::Data sender_running;
            Bool::Data receiver_running;
        } in;

    } io;

public:

    void setup(automation::test::Instance& instance) {

        auto f_write = automation::rikerio::Provider::F_Write;
        auto f_read = automation::rikerio::Provider::F_Read;

        auto& provider = context.add_provider<automation::rikerio::Provider>(instance.get_profile_path(), "test");

        provider
        .create_data<double>("sender.pdo.source", true)
        .create_data<double>("receiver.pdo.target", true);

        provider
        .create<double>("sender.pdo.source", io.source, f_write)
        .create<double>("receiver.pdo.target", io.target, f_read);

        // define test scenarios

        // on cycle 1 check that the sender and receiver are running
        /* at(1).execute([&](auto) {
            automation::test::_assert(io.in.receiver_running, "Receiver is NOT running.");
            automation::test::_assert(io.in.sender_running, "Sender is NOT running");
        }) */

        // starting with cycle 1 check that both pdos are false for the next
        at(1).execute([&](auto) {
            automation::test::_assert(io.source == 0.0d, "source not initialized.");
            automation::test::_assert(io.target == 0.0d, "target not initialized.");
        });

        // set the sender pdo to true
        at(2).execute([&](auto) {
            io.source = 3.1467453;
        });

        // check that in cycle 3 the receiver pdo is also true
        at(4).execute([&](auto) {
            automation::test::_assert(io.source == 3.1467453, "Source Value NOT Set");
            automation::test::_assert(io.target == 3.1467453, "Target Value NOT Set");
        });


    }

    void tear_down() { }

};


int main() {

    // setup test suite

    spdlog::set_level(spdlog::level::critical);

    automation::test::Suite test_suite;

    automation::sync::config::Main sync_config = {
        .thread = {
            .scheduler = {
                .type = automation::os::SchedulerType::rr
            },
            .args = {
                .realtime = {
                    .cycle = 10000,
                    .priority = 95
                }
            }
        },
        .com = {
            .cycle = 10000,
            .udp_timeout_count = 1,
            .tcp_timeout_count = 1,
            .ping_cycle_count = 5,
            .ping_timeout_count = 1,
            .max_payload_size = 1024
        },
        .sender = {
            .cycle = 10000,
            .host = "127.0.0.1",
            .port = 0, // not used
            .prefix = "sender",
            .rikerio = "test",
            .auto_ack = false
        },
        .receiver = {
            .cycle = 10000,
            .host = "127.0.0.1",
            .port = 50123,
            .prefix = "receiver",
            .rikerio = "test",
            .auto_ack = false
        }

    };

    automation::sync::config::Data sync_data_bool = {
        .item_list = {
            {
                .source = "source",
                .target = "target",
                .type = "bool"
            }
        }
    };

    automation::sync::config::Data sync_data_double = {
        .item_list = {
            {
                .source = "source",
                .target = "target",
                .type = "double"
            }
        }
    };



    auto init_sender = [&](automation::test::Instance& instance) -> automation::os::TaskInterface* {
        auto c = sync_config;
        c.sender.rikerio = instance.get_profile_path();
        return new sender::Task(c, sync_data_bool, true);
    };

    auto init_receiver = [&](automation::test::Instance& instance) -> automation::os::TaskInterface* {
        auto c = sync_config;
        c.receiver.rikerio = instance.get_profile_path();
        return new receiver::Task(c, sync_data_bool, true);
    };

    test_suite.add_test("Communication Test (Single Bool)")
    .add_subject_handler(init_receiver)
    .add_subject_handler(init_sender)
    .add_scenario<BoolSubmit>();

    auto init_sender_double = [&](automation::test::Instance& instance) -> automation::os::TaskInterface* {
        auto c = sync_config;
        c.sender.rikerio = instance.get_profile_path();
        return new sender::Task(c, sync_data_double, true);
    };

    auto init_receiver_double = [&](automation::test::Instance& instance) -> automation::os::TaskInterface* {
        auto c = sync_config;
        c.receiver.rikerio = instance.get_profile_path();
        return new receiver::Task(c, sync_data_double, true);
    };


    test_suite.add_test("Communication Test (Single Double)")
    .add_subject_handler(init_receiver_double)
    .add_subject_handler(init_sender_double)
    .add_scenario<DoubleSubmit>();

    return test_suite.start() ? EXIT_SUCCESS : EXIT_FAILURE;

}
