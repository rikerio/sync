#include "receiver/udp/server.h"

#include "stdexcept"
#include "spdlog/spdlog.h"

using namespace automation::sync::receiver::udp;

Server::Server(
    const std::string& host,
    const unsigned short port,
    const unsigned int max_receive_buffer_size) :
    host(host),
    port(port),
    max_receive_buffer_size(max_receive_buffer_size) {

    receive_buffer = (uint8_t*) calloc(1, max_receive_buffer_size + (2 * sizeof(uint16_t)));

    /* Socket erzeugen */

    if ((fd = ::socket (AF_INET, SOCK_DGRAM, 0)) < 0) {
        std::string err_msg = "Unable to open UDP socket ...(" + std::string(strerror(errno)) + ")";
        throw std::runtime_error(err_msg);
    }

    // convert string ip to uint32_t

    uint32_t bind_host = 0;
    if (inet_pton(AF_INET, host.c_str(), &bind_host) != 1) {
        throw std::runtime_error("Bind Adresse is no valid IP4 Addresse.");
    }

    spdlog::debug("receiver::udp::server Converting IP {} to uint32 = {}.", host.c_str(), bind_host);

    /* Lokalen Server Port bind(en) */
    socket_adr.sin_family = AF_INET;
    socket_adr.sin_addr.s_addr = bind_host;
    socket_adr.sin_port = htons (port);

    int reuse = 1;
    if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (const char*)&reuse, sizeof(reuse)) < 0)
        perror("setsockopt(SO_REUSEADDR) failed");

    if (setsockopt(fd, SOL_SOCKET, SO_REUSEPORT, (const char*)&reuse, sizeof(reuse)) < 0)
        perror("setsockopt(SO_REUSEPORT) failed");

    if (bind (fd, (struct sockaddr *) &socket_adr, sizeof (socket_adr)) < 0) {
        std::string err_msg = "Unable to bind UDP port ...(" + std::string(strerror(errno)) + ")";
        throw std::runtime_error(err_msg);
    }

    spdlog::debug("receiver::udp::server Creating UDP Socket successfull.");

}

void Server::close() {

    ::close(fd);

}

ssize_t Server::read_from_socket(uint8_t** buffer, uint32_t* sender_ip) {

    sockaddr_in udp_client_adr = { };
    socklen_t udp_client_adr_len = sizeof(udp_client_adr);

    ssize_t recv_len = recvfrom(
                           fd,
                           receive_buffer,
                           max_receive_buffer_size,
                           MSG_DONTWAIT,
                           (struct sockaddr *) &udp_client_adr,
                           &udp_client_adr_len);


    if (recv_len == -1 && errno == EAGAIN) {
        return 0;
    }

    if (sender_ip != nullptr) {
        *sender_ip = udp_client_adr.sin_addr.s_addr;
    }

    if (buffer != nullptr) {
        *buffer = receive_buffer;
    }

    return recv_len;

}
