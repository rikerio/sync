#ifndef __AUTOMATION_UTILS_EVENT_EMITTER_H__
#define __AUTOMATION_UTILS_EVENT_EMITTER_H__

#include "functional"
#include "map"

namespace automation::utils {

class EventHandler {
public:

    using Handler = std::function<void()>;

    EventHandler() {

    }

    EventHandler& operator <<(Handler handler) {
        handler_list.push_back(handler);
        return *this;
    }

    void emit() {
        for (auto h : handler_list) {
            h();
        }
    }

private:
    std::vector<Handler> handler_list;
};

class EventEmitter {

public:

    using Handler = std::function<void()>;

    template<typename T>
    void on(T event, Handler handler) {
        event_handler_map[static_cast<unsigned int>(event)] << handler;
    }

    void on(unsigned int event, Handler handler) {
        event_handler_map[event] << handler;
    }

    template<typename T>
    EventHandler& on(T event) {
        return event_handler_map[static_cast<unsigned int>(event)];
    }


    EventHandler& on(unsigned int event) {
        return event_handler_map[event];
    }

    template<typename T>
    void emit(T event) {
        event_handler_map[static_cast<unsigned int>(event)].emit();
    }

    void emit(unsigned int event) {
        event_handler_map[event].emit();
    }

private:

    std::map<unsigned int, EventHandler> event_handler_map;

};

}


#endif
