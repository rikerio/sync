#include "control-task.h"
#include "spdlog/spdlog.h"

using namespace automation::sync;

ControlTask::ControlTask(
    const std::string& rikerio_profile,
    const std::string& rikerio_prefix,
    automation::os::CyclicThreadInterface& thread) :
    thread(thread),
    context() {

    auto& io_provider = context.add_provider<automation::rikerio::Provider>(
                            rikerio_profile,
                            rikerio_prefix + ".ctrl",
                            rikerio_prefix + ".ctrl",
                            rikerio_prefix + ".ctrl");


    const auto f_read_write = automation::rikerio::Provider::F_Read |
                              automation::rikerio::Provider::F_Write;

    const auto f_write = automation::rikerio::Provider::F_Write;

    io_provider
    .create_data<uint16_t>(".command", true)
    .create_data<uint8_t>(".state", true)
    .create_data<uint32_t>(".stat.cycle.max", true)
    .create_data<uint32_t>(".stat.cycle.last", true)
    .create_data<uint32_t>(".stat.cycle.avg", true)
    .create_data<uint32_t>(".stat.cycle.scheduled", true)
    .create<uint16_t>(".command",  io_inout_command, f_read_write)
    .create<uint8_t>(".state", io_out_state, f_write)
    .create<uint32_t>(".stat.cycle.max", io_out_max_cycle, f_write)
    .create<uint32_t>(".stat.cycle.last", io_out_last_cycle, f_write)
    .create<uint32_t>(".stat.cycle.avg", io_out_avg_cycle, f_write)
    .create<uint32_t>(".stat.cycle.scheduled", io_out_scheduled_cycle, f_write);


    //module_command.on(Module::Command<uint16_t>::Event::Received)
    //        << bind(&ControlTask::handle_command, this);

}

void ControlTask::init() {

    spdlog::info("Initializing Control-Task.");

    context.init();

    io_out_state = 1;

    spdlog::info("Control-Task initialized.");

}

void ControlTask::loop() {

    //std::cout << Runtime::Debug::get_milliseconds() << " - ControlTask::loop" << std::endl;

    context.loop([&]() {

        if (thread.is_running()) {
            io_out_state = 2;
        } else {
            io_out_state = 1;
        }

        //module_command.set_command(io_inout_command);
        //module_command.loop();

        auto& m = thread.get_measurement();

        //io_inout_command = module_command.get_reset_value();
        io_out_last_cycle = m.get_last();
        io_out_max_cycle = m.get_max();
        io_out_avg_cycle = m.get_average();
        io_out_scheduled_cycle = m.get_cycle();
    });

}

void ControlTask::quit() {

    io_out_state = 0;

    context.quit();

}

void ControlTask::handle_command() {

//    uint16_t id = module_command.get_command();

    uint16_t id = 0;

    spdlog::info("New command coming in {}.", id);

    switch (id) {
    case (1):
        if (!thread.is_running()) {
            thread.start();
        }
        break;
    case (2):
        if (thread.is_running()) {
            thread.stop();
        }
        break;
    case (3): // reset measurement
        thread.get_measurement().reset();
        break;
    }

}
