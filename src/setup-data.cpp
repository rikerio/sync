#include "setup-data.h"
#include "config/main.h"
#include "config/data.h"

using namespace automation::sync;

SetupData::SetupData(config::Participant config_main, config::Data config_data, bool sender) :
    sender(sender),
    config_main(config_main),
    config_data(config_data),
    context(),
    provider(context.add_provider<automation::rikerio::Provider>(
                 config_main.rikerio,
                 config_main.prefix + ".pdo.",
                 config_main.prefix + ".pdo.",
                 config_main.prefix + ".pdo.")) {

}

void SetupData::init() {
    static const std::map<const std::string, const Type> Type_str_map{
        { "bool", Type::Bool },
        { "uint8", Type::Uint8 },
        { "int8", Type::Int8 },
        { "uint16", Type::Uint16 },
        { "int16", Type::Int16 },
        { "uint32", Type::Uint32 },
        { "int32", Type::Int32 },
        { "uint64", Type::Uint64 },
        { "int64", Type::Int64 },
        { "float", Type::Float },
        { "double", Type::Double }
    };


    for (unsigned int idx = 0; idx < config_data.item_list.size(); idx += 1) {

        auto& item = config_data.item_list[idx];
        auto& type = Type_str_map.at(item.type);
        auto& id = sender ? item.source : item.target;

        switch (type) {
        case (Type::Bool):
            provider.create_data<bool>(id, true);
            break;
        case (Type::Uint8):
            provider.create_data<uint8_t>(id, true);
            break;
        case (Type::Int8):
            provider.create_data<int8_t>(id, true);
            break;
        case (Type::Uint16):
            provider.create_data<uint16_t>(id, true);
            break;
        case (Type::Int16):
            provider.create_data<int16_t>(id, true);
            break;
        case (Type::Uint32):
            provider.create_data<uint32_t>(id, true);
            break;
        case (Type::Int32):
            provider.create_data<int32_t>(id, true);
            break;
        case (Type::Uint64):
            provider.create_data<uint64_t>(id, true);
            break;
        case (Type::Int64):
            provider.create_data<int64_t>(id, true);
            break;
        case (Type::Float):
            provider.create_data<float>(id, true);
            break;
        case (Type::Double) :
            provider.create_data<double>(id, true);
            break;
        }
    }

    context.init();
}

void SetupData::quit() {
    context.quit();
}
