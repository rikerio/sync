#include "receiver/tcp/server.h"
#include "stdexcept"
#include "fcntl.h"
#include "spdlog/spdlog.h"

using namespace automation::sync::receiver::tcp;

Server::Server(const std::string& host, const unsigned short port, const size_t max_buffer_size) :
    host(host),
    port(port),
    max_buffer_size(max_buffer_size) {

    server_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (server_fd == -1) {
        std::string msg = "Creating TCP Socket failed (" + std::string(strerror(errno)) + ").";
        throw std::runtime_error(msg);
    }

    bzero(&socket_adr, sizeof(socket_adr));

    // convert string ip to uint32_t

    uint32_t bind_host = 0;
    if (inet_pton(AF_INET, host.c_str(), &bind_host) != 1) {
        throw std::runtime_error("Bind Adresse is no valid IP4 Addresse.");
    }

    spdlog::debug("receiver::tcp::server Converting IP {} to uint32 = {}.", host.c_str(), bind_host);

    // assign IP, PORT
    socket_adr.sin_family = AF_INET;
    socket_adr.sin_addr.s_addr = bind_host;
    socket_adr.sin_port = htons(port);

    int reuse = 1;
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, (const char*)&reuse, sizeof(reuse)) < 0)
        perror("setsockopt(SO_REUSEADDR) failed");

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEPORT, (const char*)&reuse, sizeof(reuse)) < 0)
        perror("setsockopt(SO_REUSEPORT) failed");

    // Binding newly created socket to given IP and verification
    if ((bind(server_fd, (const sockaddr*) &socket_adr, sizeof(socket_adr))) != 0) {
        std::string msg = "Binding TCP Socket failed (" + std::string(strerror(errno)) + ").";
        throw std::runtime_error(msg);
    }

    int tmp_fd = fcntl( server_fd, F_GETFL );
    tmp_fd |= O_NONBLOCK;
    fcntl( server_fd, F_SETFL, tmp_fd );

    spdlog::debug("receiver::tcp::server Creating TCP Socket successfull.");

}

Server::~Server() {

    ::close(client_fd);
    ::close(server_fd);

}

void Server::close() {

    ::close(server_fd);
    ::close(client_fd);

}

void Server::set_whitelist_ip(const std::string& ip) {

    if (inet_pton(AF_INET, ip.c_str(), &whitelist_ip) != 1) {
        throw std::runtime_error("Adresse is no valid IP4 Addresse.");
    }

}

void Server::wait_for_client(bool& client_connected) {

    sockaddr_in client_adr = { };
    socklen_t len = sizeof(client_adr);

    // Now server is ready to listen and verification
    if ((listen(server_fd, 1)) != 0) {
        spdlog::error("receiver::tcp::server Error listening to server socket ({}).", strerror(errno));
        client_connected = false;
        return;
    }

    // Accept the data packet from client and verification
    int tmp_client_fd = accept(server_fd, (sockaddr*) &client_adr, &len);
    if (tmp_client_fd < 0) {
        if (errno == EAGAIN) {
            client_connected = false;
            return;
        } else {
            spdlog::error("receiver::tcp::server Error accepting connections on TCP server socket ({}).", strerror(errno));
            client_connected = false;
            return;
        }
    }

    if (connected_client_count == 1) {
        spdlog::error("receiver::tcp::server Client already connected, closing client connection.");
        ::close(tmp_client_fd);
        client_connected = false;
        return;
    }

    if (client_adr.sin_addr.s_addr != whitelist_ip) {
        spdlog::error("receiver::tcp::server Client not whitelisted ip, closing client connection.");
        ::close(tmp_client_fd);
        client_connected = false;
        return;
    }

    // socket accepted

    spdlog::debug("receiver::tcp::server Client accepted.");
    client_connected = true;
    client_fd = tmp_client_fd;
    connected_client_count += 1;

}

void Server::write_to_socket(uint8_t* msg, size_t msg_size) {

    if (connected_client_count == 0) {
        return;
    }

    ssize_t res = send(client_fd, msg, msg_size, MSG_NOSIGNAL | MSG_DONTWAIT);

    if (res == -1) {

        if (errno == EAGAIN) {
            return;
        }

        if (errno == EPIPE) {
            // handle closed connection
            connected_client_count = 0;
            return;
        }

    }

}

void Server::write_ping() {

    spdlog::debug("receiver::tcp::server Sending TCP ping.");

    uint16_t cmd = htons(0xFFFE);

    write_to_socket((uint8_t*) &cmd, sizeof(cmd));

}

void Server::write_reset() {

    spdlog::debug("receiver::tcp::server Sending TCP reset.");

    uint16_t cmd = htons(0x0001);

    write_to_socket((uint8_t*) &cmd, sizeof(cmd));

}

unsigned int Server::get_client_count() const {
    return connected_client_count;
}
