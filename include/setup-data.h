#ifndef __AUTOMATION_SYNC_SETUP_DATA_H__
#define __AUTOMATION_SYNC_SETUP_DATA_H__

#include "rikerio-automation/automation.h"
#include "config/data.h"
#include "config/main.h"

namespace automation::sync {

class SetupData {

public:
    SetupData(config::Participant, config::Data, bool);

    void init();
    void quit();

private:

    enum class Type {
        Bool, Uint8, Int8, Uint16, Int16, Uint32, Int32, Uint64, Int64, Float, Double
    };

    bool                            sender = false;
    config::Participant             config_main;
    config::Data                    config_data;
    automation::Context             context;
    automation::rikerio::Provider&  provider;

};

}


#endif
