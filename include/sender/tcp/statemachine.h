#ifndef __RIKERIO_SYNC_SENDER_TCP_STATEMACHINE_H__
#define __RIKERIO_SYNC_SENDER_TCP_STATEMACHINE_H__

#include "rikerio-automation/automation.h"
#include "utils/event-emitter.h"
#include "utils/statemachine.h"

namespace automation::sync::sender::tcp {

class StateMachine : public utils::EventEmitter {

public:

    enum class Event {
        Connect, Wait, Receive, Reset, Ping, Error, Closed
    };

    StateMachine(unsigned int, bool);

    void ack();
    void conn_est();
    void conn_end();
    void conn_err();
    void recv_rst();
    void recv_png();

    void loop();

private:

    enum class State {
        Closed, Ready, Wait, Error
    };

    utils::StateMachine<State> 	sm;

    bool						auto_ack = false;
    bool						cmd_ack = false;
    bool						cmd_conn_est = false;
    bool						cmd_conn_end = false;
    bool						cmd_conn_err = false;
    bool						cmd_recv_rst = false;
    bool						cmd_recv_png = false;

    unsigned int				timeout_receive;
    utils::Timer				timer_receive;
    utils::Timer				timer_wait;

private:

    void on_closed();
    void on_ready();
    void on_wait();
    void on_error();

};

}


#endif
