#include "config/main.h"
#include "yaml-cpp/yaml.h"
#include "config/exceptions.h"

using namespace automation::sync::config;

static void deserialize(const std::string& filename, Com& com) {

    YAML::Node root_node;

    try {
        root_node = YAML::LoadFile(filename);
    } catch (YAML::BadFile& e) {
        throw Error("File not found or not readable.");
    } catch (YAML::ParserException& e) {
        throw Error(e.what());
    } catch (YAML::BadConversion& e) {
        throw Error(e.what());
    } catch (...) {
        throw Error("Unknown exception occured.");
    }

    auto n_cycle = root_node["cycle"];
    auto n_udp_timeout_count = root_node["udp_timeout_count"];
    auto n_tcp_timeout_count = root_node["tcp_timeout_count"];
    auto n_ping_cycle_count = root_node["ping_cycle_count"];
    auto n_ping_timeout_count = root_node["ping_timeout_count"];
    auto n_max_payload_size = root_node["max_payload_size"];

    if (n_cycle && n_cycle.Type() == YAML::NodeType::Scalar) {
        com.cycle = n_cycle.as<unsigned int>(100) * 1000;
    }

    if (n_udp_timeout_count && n_udp_timeout_count.Type() == YAML::NodeType::Scalar) {
        com.udp_timeout_count = n_udp_timeout_count.as<unsigned int>(1000);
    }

    if (n_tcp_timeout_count && n_tcp_timeout_count.Type() == YAML::NodeType::Scalar) {
        com.tcp_timeout_count = n_tcp_timeout_count.as<unsigned int>(1000);
    }

    if (n_ping_cycle_count && n_ping_cycle_count.Type() == YAML::NodeType::Scalar) {
        com.ping_cycle_count = n_ping_cycle_count.as<unsigned int>(10);
    }

    if (n_ping_timeout_count && n_ping_timeout_count.Type() == YAML::NodeType::Scalar) {
        com.ping_timeout_count = n_ping_timeout_count.as<unsigned int>(5);
    }

    if (n_max_payload_size && n_max_payload_size.Type() == YAML::NodeType::Scalar) {
        com.max_payload_size = n_max_payload_size.as<unsigned int>(4096);
    }

}

static void deserialize(const std::string&, automation::os::ThreadConfig& thread) {

    thread.scheduler.type = automation::os::SchedulerType::rr;
    thread.args.realtime.priority = 95;

}

static void deserialize_sender(const std::string& filename, Participant& sender) {

    YAML::Node root_node;

    try {
        root_node = YAML::LoadFile(filename);
    } catch (YAML::BadFile& e) {
        throw Error("File not found or not readable.");
    } catch (YAML::ParserException& e) {
        throw Error(e.what());
    } catch (YAML::BadConversion& e) {
        throw Error(e.what());
    } catch (...) {
        throw Error("Unknown exception occured.");
    }


    auto n_sender = root_node["sender"];


    if (!n_sender || n_sender.Type() != YAML::NodeType::Map) {
        throw Error("Missing Sender in configuration.");
    }

    auto n_sender_host = n_sender["host"];
    auto n_sender_prefix = n_sender["prefix"];
    auto n_sender_rikerio = n_sender["rikerio"];
    auto n_sender_auto_ack = n_sender["auto_ack"];

    if (!n_sender_host || n_sender_host.Type() != YAML::NodeType::Scalar) {
        throw Error("Missing host in sender configuration.");
    }

    if (!n_sender_prefix || n_sender_prefix.Type() != YAML::NodeType::Scalar) {
        throw Error("Missing prefix in sender configuration.");
    }

    if (!n_sender_rikerio || n_sender_rikerio.Type() != YAML::NodeType::Scalar) {
        throw Error("Missing rikerio in sender configuration.");
    }

    if (!n_sender_auto_ack || n_sender_auto_ack.Type() != YAML::NodeType::Scalar) {
        throw Error("Missing auto_ack in sender configuration.");
    }

    sender.host = n_sender_host.as<std::string>();
    sender.prefix = n_sender_prefix.as<std::string>();
    sender.rikerio = n_sender_rikerio.as<std::string>();
    sender.auto_ack = n_sender_auto_ack.as<bool>();


}


static void deserialize_receiver(const std::string& filename, Participant& receiver) {

    YAML::Node root_node;

    try {
        root_node = YAML::LoadFile(filename);
    } catch (YAML::BadFile& e) {
        throw Error("File not found or not readable.");
    } catch (YAML::ParserException& e) {
        throw Error(e.what());
    } catch (YAML::BadConversion& e) {
        throw Error(e.what());
    } catch (...) {
        throw Error("Unknown exception occured.");
    }

    auto n_receiver = root_node["receiver"];


    if (!n_receiver || n_receiver.Type() != YAML::NodeType::Map) {
        throw Error("Missing Receiver in configuration.");
    }

    auto n_receiver_host = n_receiver["host"];
    auto n_receiver_port = n_receiver["port"];
    auto n_receiver_prefix = n_receiver["prefix"];
    auto n_receiver_rikerio = n_receiver["rikerio"];
    auto n_receiver_auto_ack = n_receiver["auto_ack"];

    if (!n_receiver_host || n_receiver_host.Type() != YAML::NodeType::Scalar) {
        throw Error("Missing host in receiver configuration.");
    }

    if (!n_receiver_port || n_receiver_port.Type() != YAML::NodeType::Scalar) {
        throw Error("Missing port in receiver configuration.");
    }

    if (!n_receiver_prefix || n_receiver_prefix.Type() != YAML::NodeType::Scalar) {
        throw Error("Missing prefix in receiver configuration.");
    }

    if (!n_receiver_rikerio || n_receiver_rikerio.Type() != YAML::NodeType::Scalar) {
        throw Error("Missing rikerio in receiver configuration.");
    }

    if (!n_receiver_auto_ack || n_receiver_auto_ack.Type() != YAML::NodeType::Scalar) {
        throw Error("Missing auto_ack in receiver configuration.");
    }

    receiver.host = n_receiver_host.as<std::string>();
    receiver.port = n_receiver_port.as<unsigned int>();
    receiver.prefix = n_receiver_prefix.as<std::string>();
    receiver.rikerio = n_receiver_rikerio.as<std::string>();
    receiver.auto_ack = n_receiver_auto_ack.as<bool>();

}

void Main::deserialize(const std::string& filename, Main& main) {

    ::deserialize(filename, main.com);
    ::deserialize(filename, main.thread);
    ::deserialize_sender(filename, main.sender);
    ::deserialize_receiver(filename, main.receiver);

    if (main.thread.scheduler.type == automation::os::SchedulerType::rr) {
        main.thread.args.realtime.cycle = main.com.cycle;
    }

}
