#ifndef __STAGEAUTOMATION_UTILS_STATEMACHINE_H__
#define __STAGEAUTOMATION_UTILS_STATEMACHINE_H__

#include "type_traits"
#include "set"
#include "functional"
#include "map"

namespace automation::utils {

template<typename T>
class StateMachine {

public:

    using StateHandler = std::function<void()>;
    using TransitionHandler = std::function<void(T,T)>;

public:

    StateMachine(T initial_state) :
        last_state(initial_state),
        current_state(initial_state),
        next_state(initial_state) { }

    T get_last_state() const {
        return last_state;
    }

    T get_current_state() const {
        return current_state;
    }

    T get_next_state() const {
        return next_state;
    }

    bool is_first_call() const {
        return first_call;
    }

    void set_next_state(T state) {
        next_state = state;
    }

    bool in(T state) const {
        return current_state == state;
    }

    bool in(std::set<T> states) const {
        for (auto s : states) {
            if (current_state == s) {
                return true;
            }
        }

        return false;

    }

    bool execute_transition() {

        first_call = current_state != next_state || first;

        first = false;

        last_state = current_state;

        call_transition_handler();

        current_state = next_state;

        call_state_handler();

        return first_call;
    }

    StateMachine& on(T state, StateHandler handler) {
        state_handler[state].push_back(handler);
        return *this;
    }

    StateMachine& on(TransitionHandler handler) {
        transition_list.push_back(handler);
        return *this;
    }


private:


    T last_state;
    T current_state;
    T next_state;

    bool first_call = false;
    bool first = true;

private:

    using HandlerList = std::vector<StateHandler>;
    using HandlerMap = std::map<T, HandlerList>;
    using TransitionList = std::vector<TransitionHandler>;

    HandlerMap 				state_handler;
    TransitionList		    transition_list;

private:

    void call_state_handler() {

        for (auto h : state_handler[current_state]) {
            h();
        }

    }

    void call_transition_handler() {

        for (auto h : transition_list) {
            h(current_state, next_state);
        }

    }

};

};

#endif
