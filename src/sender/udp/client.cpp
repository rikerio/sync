#include "sender/udp/client.h"

#include "cstring"
#include "strings.h"
#include "stdexcept"

using namespace automation::sync::sender::udp;

Client::Client(const std::string& host, uint16_t port) :
    host(host), port(port) {

    udp_fd = socket(AF_INET,SOCK_DGRAM, IPPROTO_UDP);
    if(udp_fd < 0) {
        throw std::runtime_error("Cannot open UDP socket");
    }

    uint32_t bind_host = 0;
    if (inet_pton(AF_INET, host.c_str(), &bind_host) != 1) {
        throw std::runtime_error("Bind Adresse is no valid IP4 Addresse.");
    }

    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = bind_host;
    servaddr.sin_port = htons(port);

}

Client::~Client() {
    close(udp_fd);
}

void Client::send_message(uint8_t* msg, size_t size) {

    socklen_t socklen = sizeof(servaddr);

    if (sendto(udp_fd, msg, size, MSG_DONTWAIT, (sockaddr*)&servaddr, socklen) == -1) {

        std::string err_msg = "Error sending UDP message (" + std::string(strerror(errno)) + ").";

        throw send_error(err_msg);

    }

}
