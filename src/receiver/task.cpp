#include "receiver/task.h"

#include "stdexcept"
#include "spdlog/spdlog.h"

using namespace automation::sync::receiver;

Task::Task(config::Main& config_main,
           config::Data& config_data,
           bool test) :
    config_main(config_main),
    config_data(config_data),
    sm(config_main.receiver.auto_ack),
    udp_server(
        config_main.receiver.host,
        config_main.receiver.port,
        config_main.com.max_payload_size),
    udp_sm(
        config_main.com.cycle * config_main.com.udp_timeout_count,
        config_main.receiver.auto_ack),
    tcp_server(
        config_main.receiver.host,
        config_main.receiver.port,
        config_main.com.max_payload_size),
    tcp_sm(config_main.com.ping_cycle_count) {

    /* const auto f_read = automation::rikerio::Provider::F_Read;
    const auto f_write = automation::rikerio::Provider::F_Write; */

    std::filesystem::path rikerio_profile_path = std::filesystem::path(RikerIO::root_path) / config_main.sender.rikerio;

    if (test) {
        rikerio_profile_path = std::filesystem::path(config_main.receiver.rikerio);
    }

    auto& io_provider = context.add_provider<automation::rikerio::Provider>(
                            rikerio_profile_path,
                            config_main.receiver.prefix + ".pdo",
                            config_main.receiver.prefix + ".pdo.",
                            config_main.receiver.prefix + ".pdo.");

//    io_provider
//    .create_data<bool>("in.ack", true)
//    .create_data<bool>("out.flag.running", true)
//    .create_data<bool>("out.flag.reqack", true)
//    .create_data<bool>("out.flag.error", true)

    /* Move this to the control task, handle only PDOs in here
     *
    .create<bool>("in.ack", io.in.ack, f_read)
    .create<bool>("out.flag.running", io.out.running, f_write)
    .create<bool>("out.flag.reqack", io.out.reqack, f_write)
    .create<bool>("out.flag.error", io.out.error, f_write); */

    tcp_server.set_whitelist_ip(config_main.sender.host);

    // create write list

    for (unsigned int idx = 0; idx < config_data.item_list.size(); idx += 1) {

        auto& item = config_data.item_list[idx];

        auto write_job = std::make_shared<WriteJob>(
                             io_provider,
                             item.target,
                             WriteJob::Type_str_map.at(item.type));

        write_list.push_back(write_job);

    }

    sm.on(StateMachine::Event::Start) << std::bind(&tcp::Server::write_reset, &tcp_server);

    udp_sm.on(udp::StateMachine::Event::Wait) << std::bind(&Task::udp_reset, this);
    udp_sm.on(udp::StateMachine::Event::Wait) << std::bind(&Task::udp_receive, this);
    udp_sm.on(udp::StateMachine::Event::Read) << std::bind(&Task::udp_receive, this);

    tcp_sm.on(tcp::StateMachine::Event::Wait) << std::bind(&Task::tcp_wait, this);
    tcp_sm.on(tcp::StateMachine::Event::Ready) << std::bind(&Task::tcp_ready, this);
    tcp_sm.on(tcp::StateMachine::Event::SendPing) << std::bind(&tcp::Server::write_ping, &tcp_server);

    trigger_ack.on(utils::Trigger::Event::Rising)
            << std::bind(&udp::StateMachine::ack, &udp_sm)
            << std::bind(&StateMachine::ack, &sm);

}

Task::~Task() {}


void Task::init() {

    spdlog::info("receiver::task Initializing.");

    try {
        context.init();
    } catch (std::exception& e) {
        spdlog::error("{}", e.what());
    }

    io.out.running = true;

    spdlog::info("receiver::task initialized.");

}

void Task::quit() {

    spdlog::info("receiver::task Stopping.");

    io.out.running = false;

    tcp_server.close();
    udp_server.close();
    context.quit();

    spdlog::info("receiver::task stopped.");

}

void Task::loop() {

    context.loop([&]() {

        trigger_ack.set_value(io.in.ack);

        trigger_ack.loop();

        udp_sm.loop();
        tcp_sm.loop();

        spdlog::debug("receiver::task tcp_ready = {}. udp_ready = {}.", tcp_sm.is_ready(), udp_sm.is_ready());

        sm.set_tcp_ready(tcp_sm.is_ready());
        sm.set_udp_ready(udp_sm.is_ready());

        sm.loop();

        io.out.error = udp_sm.is_error();
        io.out.reqack = udp_sm.is_reqack();

    });

}

void Task::udp_reset() {

    udp_expected_id = 0;

}

void Task::udp_receive() {

    uint8_t* udp_receive_buffer = nullptr;
    uint32_t udp_receive_host = 0;
    ssize_t udp_recv_len = udp_server.read_from_socket(&udp_receive_buffer, &udp_receive_host);

    if (udp_recv_len < 0) {
        spdlog::error("receiver::task Error receiving data from udp socket ({}).", strerror(errno));
        return;
    }

    if (udp_recv_len == 0) {
        return;
    }

    spdlog::debug("receiver::task Receive response from UDP socket = {}.", udp_recv_len);

    // TODO: check client ip

    udp_deserialize(udp_receive_buffer, udp_recv_len);

    udp_expected_id += 1;

    if (udp_expected_id == 0) {
        udp_expected_id = 1;
    }

}

void Task::udp_deserialize(uint8_t* udp_receive_buffer, size_t udp_recv_len) {

    uint16_t udp_id = ntohs(*(uint16_t*) udp_receive_buffer);
    uint16_t payload_len = ntohs(*(uint16_t*) (udp_receive_buffer + 2));

    if (udp_id != udp_expected_id) {
        spdlog::debug("receiver::task Received UDP message with unexpected ID {} != {}.", udp_expected_id, udp_id);
        return;
    }

    spdlog::debug("receiver::task Received UDP message with expected ID {} and payload len {}.", udp_id, payload_len);

    size_t byte_cntr = 4;
    uint8_t* ptr = udp_receive_buffer + 4;
    while (byte_cntr < udp_recv_len) {

        uint16_t udp_job_idx = ntohs(*(uint16_t*) ptr);
        uint8_t* udp_value_ptr = ptr + sizeof(uint16_t);

        if (udp_job_idx >= write_list.size()) {
            spdlog::error("receiver::task No such index on write list, probably malformed udp packed.");
            return;
        }

        spdlog::debug("receiver::task updating write job at index {}.", udp_job_idx);

        auto write_job = write_list[udp_job_idx];

        write_job->apply(udp_value_ptr);

        size_t job_size = write_job->get_byte_size();

        ptr += sizeof(uint16_t) + job_size;

        byte_cntr += sizeof(uint16_t) + job_size;

    }

    if (udp_id == 0) {
        udp_sm.start();
    } else {
        udp_sm.valid_msg_received();
    }

}

void Task::tcp_wait() {

    bool is_client_connected = false;

    tcp_server.wait_for_client(is_client_connected);

    if (is_client_connected) {
        tcp_sm.client_connected();
    }

}

void Task::tcp_ready() {

    if (tcp_server.get_client_count() == 0) {
        tcp_sm.client_disconnected();
    } else {
        udp_sm.tcp_ready();
    }

}
