#include "sender/task.h"

#include "spdlog/spdlog.h"

using namespace automation::sync::sender;

Task::Task(
    config::Main& config_main,
    config::Data& config_data,
    bool test) :
    config_main(config_main),
    config_data(config_data),
    udp_client(config_main.receiver.host, config_main.receiver.port),
    tcp_sm(
        config_main.com.cycle * config_main.com.ping_cycle_count * config_main.com.ping_timeout_count,
        config_main.receiver.auto_ack),
    tcp_client(config_main.receiver.host, config_main.receiver.port) {

    // auto f_write = automation::rikerio::Provider::F_Write;

    std::filesystem::path rikerio_profile_path = std::filesystem::path(RikerIO::root_path) / config_main.sender.rikerio;

    if (test) {
        rikerio_profile_path = std::filesystem::path(config_main.sender.rikerio);
    }

    auto& io_provider = ctx.add_provider<automation::rikerio::Provider>(
                            rikerio_profile_path,
                            config_main.sender.prefix + ".pdo",
                            config_main.sender.prefix + ".pdo.",
                            config_main.sender.prefix + ".pdo.");

//    io_provider
//    .create_data<bool>("in.ack", true)
//    .create_data<bool>("out.flag.running", true)
//    .create_data<bool>("out.flag.reqack", true)
//    .create_data<bool>("out.flag.error", true)
//
    /* Move this to the control task, only pdos in this task
    .create<bool>("out.flag.running", io.out.flag.running, f_write); */

    udp_msg = (uint8_t*) calloc(1, config_main.com.max_payload_size);

    auto& data_list = config_data.item_list;

    for (unsigned int i = 0; i < data_list.size(); i += 1) {

        auto& data_list_item = data_list[i];

        auto read_job = std::make_shared<ReadJob>(
                            io_provider,
                            data_list_item.source,
                            ReadJob::Type_str_map.at(data_list_item.type));

        read_job_list.push_back(read_job);

        all_job_count_size += read_job->get_byte_size();

    }

    udp_sm.on(udp::StateMachine::Event::Wait) << [&]() {
        udp_id = 0;
        udp_init_job_count = 0;
    };

    udp_sm.on(udp::StateMachine::Event::Write) << [&]() {
        if (!udp_send_error) {
            prep_udp_update();
        }
        send_udp_update();
    };

    tcp_sm.on(tcp::StateMachine::Event::Connect) << std::bind(&Task::tcp_connect, this);
    tcp_sm.on(tcp::StateMachine::Event::Wait) << std::bind(&Task::tcp_wait_connect, this);
    tcp_sm.on(tcp::StateMachine::Event::Receive) << std::bind(&Task::tcp_receive, this);
    tcp_sm.on(tcp::StateMachine::Event::Reset) << std::bind(&Task::tcp_reset, this);
    tcp_sm.on(tcp::StateMachine::Event::Ping) << std::bind(&Task::tcp_ping, this);
    tcp_sm.on(tcp::StateMachine::Event::Error) << std::bind(&Task::tcp_error, this);
    tcp_sm.on(tcp::StateMachine::Event::Closed) << std::bind(&Task::tcp_closed, this);

}

Task::~Task() {

}


void Task::init() {

    spdlog::info("sender::task initializing");

    try {
        ctx.init();
    } catch (std::exception& e) {
        spdlog::error("{}", e.what());
    }

    io.out.flag.running = true;

    spdlog::info("sender::task initialized.");

}

void Task::quit() {

    spdlog::info("sender::task Stopping.");

    io.out.flag.running = false;

    tcp_client.close();
    ctx.quit();

    spdlog::info("sender::task stopped.");

}

void Task::loop() {

    ctx.loop([&]() {

        udp_sm.loop();
        tcp_sm.loop();

    });

}

void Task::prep_udp_update() {

    udp_msg_size = 0;

    size_t udp_msg_pos = 4;

    /* setup header data */

    uint16_t* udp_msg_id = (uint16_t*) &udp_msg[0];
    *udp_msg_id = htons(udp_id);

    uint16_t payload_size = 0;

    /* put only updated read jobs in */

    for (unsigned int i = 0; i < read_job_list.size(); i += 1) {

        auto read_job = read_job_list[udp_current_read_job_index];

        spdlog::debug("sender::task Handling read job {}.", udp_current_read_job_index);

        if (udp_init_job_count == read_job_list.size()) {
            if (!read_job->is_updated()) {
                spdlog::debug("sender::task Init done and not updated => skipping.");
                udp_current_read_job_index = (udp_current_read_job_index + 1) % read_job_list.size();
                continue;
            }
            spdlog::debug("sender::task Init done and updated => not skipping.");
        } else {
            spdlog::debug("sender::task Init not done => not skipping.");
        }

        uint8_t* udp_msg_ptr = &udp_msg[udp_msg_pos];
        size_t byte_size = read_job->get_byte_size();
        size_t all_size = udp_msg_pos + sizeof(uint16_t) + byte_size;

        spdlog::debug("sender::task Current size {}.", all_size);

        if (all_size > config_main.com.max_payload_size) {
            spdlog::debug("sender::task Max Payload size reached {} > {}.", all_size, config_main.com.max_payload_size);
            break;
        }

        /* apply index */

        uint16_t* tmp_msg_idx = (uint16_t*) udp_msg_ptr;
        uint16_t tmp_buf = htons(udp_current_read_job_index);
        memcpy(tmp_msg_idx, &tmp_buf, 2);
        //*tmp_msg_idx = htons(udp_current_read_job_index);

        /* apply data */

        uint8_t* tmp_data_ptr = udp_msg_ptr + sizeof(uint16_t);
        size_t job_size = read_job->apply(tmp_data_ptr);
        udp_msg_pos += job_size + sizeof(uint16_t);
        payload_size += job_size + sizeof(uint16_t);
        udp_current_read_job_index = (udp_current_read_job_index + 1) % read_job_list.size();

        /* if the udp_init_job_count is lower then the read job list size, increase
         * the value to indicate that not all read jobs have been initialy submitted. */

        if (udp_init_job_count < read_job_list.size()) {
            udp_init_job_count += 1;
        }

        read_job->update();

    }

    udp_msg_size = 2 * sizeof(uint16_t) + payload_size;

    uint16_t* udp_msg_len = (uint16_t*) &udp_msg[2];
    *udp_msg_len = htons(payload_size);

    spdlog::debug("sender::task Current UDP Message id: {}, size: {}.", udp_id, udp_msg_size);

}

void Task::send_udp_update() {

    try {

        udp_client.send_message(udp_msg, udp_msg_size);

        /* using automatic overflow */

        udp_id++;

        // id zero is used on the reset case
        if (udp_id == 0) {
            udp_id = 1;
        }

        udp_send_error = false;

    } catch (udp::Client::send_error& e) {

        /* reuse the current buffer since this might
        * not have worked out */

        spdlog::error("sender::task {}", e.what());
        udp_send_error = true;

    }

}


void Task::tcp_connect() {

    spdlog::debug("sender::task connect");

    try {

        tcp_client.connect();

    } catch (tcp::Client::connection_error& e) {

        spdlog::error("sender::task Error connecting to TCP Server ({}).", e.what());
        tcp_sm.conn_err();

    }

};

void Task::tcp_wait_connect() {

    if (tcp_client.is_connected()) {
        spdlog::debug("sender::task client connected");
        tcp_sm.conn_est();
    }

}

void Task::tcp_receive() {

    // check for ping and reset messages

    uint16_t msg = 0;
    size_t msg_size = 0;

    try {
        tcp_client.read_message((uint8_t*) &msg, &msg_size, sizeof(uint16_t));

        msg = ntohs(msg);

        if (msg_size == 0) {

            spdlog::debug("sender::Task No new message on the TCP Socket.");
            return;

        }

        if (msg_size == sizeof(msg)) {

            spdlog::debug("sender::task Received TCP message.");

            // reset command
            if (msg == 0x0001) {
                spdlog::debug("sender::task Received RESET command through TCP Socket.");
                tcp_sm.recv_rst();
            } else if (msg == 0xFFFE) {
                spdlog::debug("sender::task Received PING command through TCP Socket.");
                tcp_sm.recv_png();
            }

        }

    } catch (tcp::Client::connection_error& e) {

    } catch (tcp::Client::receive_error& e) {

    }

};

void Task::tcp_reset() {

    spdlog::debug("sender::task tcp reset");

    udp_sm.start();

};

void Task::tcp_ping() {

    spdlog::debug("sender::task tcp ping");

    // do nothing

};

void Task::tcp_error() {

    spdlog::debug("sender::task tcp error");

    tcp_client.close();
    udp_sm.stop();

    // req ack flag = 1
    // error flag = 1

};

void Task::tcp_closed() {

    spdlog::debug("sender::task tcp closed");

    // req ack flag = 0
    // error flag = 0

    udp_sm.stop();

};


