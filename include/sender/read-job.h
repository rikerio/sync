#ifndef __RIKERIO_SYNC_READ_JOB_H__
#define __RIKERIO_SYNC_READ_JOB_H__

#include "rikerio-automation/automation.h"

namespace automation::sync::sender {

class ReadJob {

public:

    enum class Type {
        Bool, Uint8, Int8, Uint16, Int16, Uint32, Int32, Uint64, Int64, Float, Double
    };

    static const map<const Type, const size_t> Size_map;

    static const map<const std::string, const Type> Type_str_map;

public:

    ReadJob(automation::rikerio::Provider&, const std::string&, Type);
    ~ReadJob();

    bool is_updated() const;
    size_t apply(uint8_t*);
    size_t get_byte_size() const;

    void update();

private:

    const std::string&	id;
    const Type			type;

    automation::rikerio::Bool::Data bool_data;
    automation::rikerio::Uint8::Data uint8_data;
    automation::rikerio::Int8::Data int8_data;
    automation::rikerio::Uint16::Data uint16_data;
    automation::rikerio::Int16::Data int16_data;
    automation::rikerio::Uint32::Data uint32_data;
    automation::rikerio::Int32::Data int32_data;
    automation::rikerio::Uint64::Data uint64_data;
    automation::rikerio::Int64::Data int64_data;
    automation::rikerio::Float::Data float_data;
    automation::rikerio::Double::Data double_data;


    uint8_t*			buffer;
    size_t				byte_size;

};

}

#endif
