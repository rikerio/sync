#include "sender/tcp/client.h"

#include "string.h"
#include "stdexcept"

#include "iostream"

using namespace automation::sync::sender::tcp;

Client::Client(const std::string& host, uint16_t port) :
    host(host), port(port) {

}


void Client::connect() {

    if ((socket_fd = socket (AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0)) == -1) {
        std::string err_msg = "Unable to create TCP Socket (" + std::string(strerror(errno)) + ").";
        throw std::runtime_error(err_msg);
    }

    address.sin_family = AF_INET;
    address.sin_port = htons (port);
    inet_aton (host.c_str(), &address.sin_addr);

    int conn_ret = ::connect ( socket_fd,
                               (struct sockaddr *) &address,
                               sizeof (address));

    if (conn_ret == 0) {
        // already connected
        return;
    }

    if (conn_ret != 0 && errno != EINPROGRESS) {
        throw connection_error(std::string(strerror(errno)));
    }


}

void Client::close() {

    ::close(socket_fd);

    socket_fd = 0;

}

void Client::send_message(uint8_t* msg, size_t size) {

    if (send(socket_fd, msg, size, MSG_DONTWAIT) == -1) {
        throw send_error();
    };

}

void Client::read_message(uint8_t* msg, size_t* size, size_t max_size) {

    ssize_t tmp_size = recv(socket_fd, msg, max_size, MSG_DONTWAIT);

    if (tmp_size == -1) {
        if (errno == EAGAIN) {
            *size = 0;
            return;
        }
        if (errno == ENOTCONN) {
            throw connection_error(std::string(strerror(errno)));
        }
        throw receive_error();
    }

    *size = tmp_size;

}

bool Client::is_connected() const {
    struct sockaddr_in junk;
    socklen_t length = sizeof(junk);
    memset(&junk, 0, sizeof(junk));
    bool res = (getpeername(socket_fd, (struct sockaddr *)&junk, &length) == 0);
    return res;
}
