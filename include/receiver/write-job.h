#ifndef __RIKERIO_SYNC_RECEIVER_READ_JOB_H__
#define __RIKERIO_SYNC_RECEIVER_READ_JOB_H__

#include "rikerio-automation/automation.h"

namespace automation::sync::receiver {

class WriteJob {

public:

    enum class Type {
        Bool, Uint8, Int8, Uint16, Int16, Uint32, Int32, Uint64, Int64, Float, Double
    };

    static const map<const Type, const size_t> Size_map;

    static const map<const std::string, const Type> Type_str_map;

public:

    WriteJob(automation::rikerio::Provider&, const std::string&, Type);

    void apply(uint8_t*);

    size_t get_byte_size() const;

private:

    const std::string&	id;
    const Type			type;

    automation::rikerio::PDO<bool>::Data        bool_data;
    automation::rikerio::PDO<uint8_t>::Data     uint8_data;
    automation::rikerio::PDO<int8_t>::Data      int8_data;
    automation::rikerio::PDO<uint16_t>::Data    uint16_data;
    automation::rikerio::PDO<int16_t>::Data     int16_data;
    automation::rikerio::PDO<uint32_t>::Data    uint32_data;
    automation::rikerio::PDO<int32_t>::Data     int32_data;
    automation::rikerio::PDO<uint64_t>::Data    uint64_data;
    automation::rikerio::PDO<int64_t>::Data     int64_data;
    automation::rikerio::PDO<float>::Data       float_data;
    automation::rikerio::PDO<double>::Data      double_data;

};

}

#endif
