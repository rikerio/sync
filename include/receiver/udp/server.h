#ifndef __RIKERIO_SYNC_RECEIVER_UDP_SERVER_H__
#define __RIKERIO_SYNC_RECEIVER_UDP_SERVER_H__

#include "sys/types.h"
#include "sys/socket.h"
#include "netinet/in.h"
#include "arpa/inet.h"
#include "netdb.h"
#include "stdio.h"
#include "unistd.h"
#include "string.h"
#include "stdlib.h"
#include "errno.h"
#include "string"

namespace automation::sync::receiver::udp {

class Server {

public:

    Server(const std::string&, const unsigned short, const unsigned int);

    void close();

    ssize_t read_from_socket(uint8_t**, uint32_t*);

private:

    const std::string		host;
    const unsigned short	port;
    const unsigned int		max_receive_buffer_size;

    int						fd;
    sockaddr_in				socket_adr;
    uint8_t*				receive_buffer;

};


}

#endif
