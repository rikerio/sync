#ifndef __RIKERIO_SYNC_SENDER_UDP_CLIENT_H__
#define __RIKERIO_SYNC_SENDER_UDP_CLIENT_H__

#include "stdexcept"
#include "string"
#include "stdint.h"
#include "unistd.h"
#include "sys/types.h"
#include "sys/socket.h"
#include "arpa/inet.h"

namespace automation::sync::sender::udp {

class Client {

public:

    class send_error : public std::runtime_error {
    public:
        send_error(const std::string& msg) : std::runtime_error(msg) { };
    };

    Client(const std::string& host, uint16_t port);
    ~Client();

    void send_message(uint8_t*, size_t);

private:

    const std::string&	host;
    const uint16_t		port;

private:

    sockaddr_in			servaddr = { };
    uint32_t			server_adr = 0;
    int 				udp_fd = 0;


};

}


#endif
