#ifndef __RIKERIO_SYNC_RECEIVER_TCP_STATEMACHINE_H__
#define __RIKERIO_SYNC_RECEIVER_TCP_STATEMACHINE_H__

#include "stdint.h"
#include "utils/event-emitter.h"
#include "utils/statemachine.h"

namespace automation::sync::receiver::tcp {

class StateMachine : public utils::EventEmitter {

public:

    enum class Event {
        Wait, Ready, SendPing, SendReset
    };

    StateMachine(unsigned int);

    void client_connected();
    void client_disconnected();

    void loop();

    bool is_ready() const;

private:

    enum class State {
        WaitForClient, Ready
    };

    utils::StateMachine<State>	sm;

    void on_wait_for_client();
    void on_ready();

private:

    unsigned int				ping_cycle_count = 5;
    uint16_t 					cycle_counter = 0;
    bool						cmd_client_connect = false;
    bool						cmd_client_disconnect = false;

};


}


#endif
